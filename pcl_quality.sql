-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2018 at 02:56 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pcl_quality`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_camshaft_scrap`
--

CREATE TABLE `tbl_camshaft_scrap` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `inspector` varchar(200) NOT NULL,
  `part_no` varchar(200) NOT NULL,
  `cs` varchar(200) NOT NULL,
  `shift` varchar(200) NOT NULL,
  `c_date` varchar(200) NOT NULL,
  `total_checked` varchar(200) NOT NULL,
  `total_good` varchar(200) NOT NULL,
  `total_rej` varchar(200) NOT NULL,
  `packed` varchar(200) NOT NULL,
  `fr_cut` varchar(200) NOT NULL,
  `met_rej` varchar(200) NOT NULL,
  `total_mould` varchar(200) NOT NULL,
  `total_pouring` varchar(200) NOT NULL,
  `total_fettling` varchar(200) NOT NULL,
  `total_pattern` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_camshaft_scrap_defects`
--

CREATE TABLE `tbl_camshaft_scrap_defects` (
  `id` int(11) NOT NULL,
  `camshaft_scrap_id` varchar(200) NOT NULL,
  `imp_no` varchar(1000) NOT NULL,
  `defects` varchar(500) NOT NULL,
  `defect_name` varchar(500) NOT NULL,
  `total_defects_by` varchar(1000) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cam_fettling_deffects`
--

CREATE TABLE `tbl_cam_fettling_deffects` (
  `id` int(11) NOT NULL,
  `camshaft_scrap_id` varchar(100) NOT NULL,
  `imp_no` varchar(500) NOT NULL,
  `defect_name` varchar(1000) NOT NULL,
  `defects` varchar(1000) NOT NULL,
  `defects_sum` varchar(500) NOT NULL,
  `total` varchar(1000) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cam_melting_deffects`
--

CREATE TABLE `tbl_cam_melting_deffects` (
  `id` int(11) NOT NULL,
  `camshaft_scrap_id` varchar(100) NOT NULL,
  `imp_no` varchar(500) NOT NULL,
  `defect_name` varchar(1000) NOT NULL,
  `defects` varchar(1000) NOT NULL,
  `defects_sum` varchar(500) NOT NULL,
  `total` varchar(1000) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cam_moulding_deffects`
--

CREATE TABLE `tbl_cam_moulding_deffects` (
  `id` int(11) NOT NULL,
  `camshaft_scrap_id` varchar(100) NOT NULL,
  `imp_no` varchar(500) NOT NULL,
  `defect_name` varchar(1000) NOT NULL,
  `defects` varchar(1000) NOT NULL,
  `defects_sum` varchar(500) NOT NULL,
  `total` varchar(1000) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cam_pattern_deffects`
--

CREATE TABLE `tbl_cam_pattern_deffects` (
  `id` int(11) NOT NULL,
  `camshaft_scrap_id` varchar(100) NOT NULL,
  `imp_no` varchar(500) NOT NULL,
  `defect_name` varchar(1000) NOT NULL,
  `defects` varchar(1000) NOT NULL,
  `defects_sum` varchar(500) NOT NULL,
  `total` varchar(1000) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_on` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_chilldepth`
--

CREATE TABLE `tbl_hardness_chilldepth` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(100) NOT NULL,
  `c_date` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `heat_no` varchar(100) NOT NULL,
  `L_C` varchar(100) NOT NULL,
  `data_value` varchar(1000) NOT NULL,
  `data_value1` varchar(1000) NOT NULL,
  `created_on` varchar(10) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_chilldepth_b`
--

CREATE TABLE `tbl_hardness_chilldepth_b` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(100) NOT NULL,
  `c_date` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `heat_no` varchar(100) NOT NULL,
  `L_C` varchar(100) NOT NULL,
  `data_value` varchar(1000) NOT NULL,
  `created_on` varchar(10) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_chilldepth_para`
--

CREATE TABLE `tbl_hardness_chilldepth_para` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(200) NOT NULL,
  `item` varchar(200) NOT NULL,
  `c_date` varchar(200) NOT NULL,
  `heat_no` varchar(200) NOT NULL,
  `at_point` varchar(200) NOT NULL,
  `cust_spec` varchar(200) NOT NULL,
  `working_spec` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_chilldepth_para_b`
--

CREATE TABLE `tbl_hardness_chilldepth_para_b` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(200) NOT NULL,
  `item` varchar(200) NOT NULL,
  `c_date` varchar(200) NOT NULL,
  `heat_no` varchar(200) NOT NULL,
  `at_point` varchar(200) NOT NULL,
  `cust_spec` varchar(200) NOT NULL,
  `working_spec` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_main`
--

CREATE TABLE `tbl_hardness_main` (
  `id` int(11) NOT NULL,
  `item` varchar(200) NOT NULL,
  `heat_no` varchar(200) NOT NULL,
  `c_date` varchar(200) NOT NULL,
  `part_no` varchar(100) NOT NULL,
  `approved_by` varchar(200) NOT NULL,
  `checked_by` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL,
  `remark` varchar(2000) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_observation`
--

CREATE TABLE `tbl_hardness_observation` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(200) NOT NULL,
  `L` varchar(1000) NOT NULL,
  `L_C` varchar(1000) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hardness_observation_b`
--

CREATE TABLE `tbl_hardness_observation_b` (
  `id` int(11) NOT NULL,
  `hardness_chill_id` varchar(200) NOT NULL,
  `L` varchar(1000) NOT NULL,
  `L_C` varchar(1000) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_spec`
--

CREATE TABLE `tbl_item_spec` (
  `id` int(11) NOT NULL,
  `item` varchar(200) NOT NULL,
  `at_point` varchar(500) NOT NULL,
  `cust_spec` varchar(200) NOT NULL,
  `working_spec` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rejection_analysis`
--

CREATE TABLE `tbl_rejection_analysis` (
  `id` int(11) NOT NULL,
  `DEFECT` varchar(100) NOT NULL,
  `Rejected_qty` varchar(100) NOT NULL,
  `Rejn` varchar(100) NOT NULL,
  `contribution` varchar(100) NOT NULL,
  `Cumm_Contribution` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rejection_analysis`
--

INSERT INTO `tbl_rejection_analysis` (`id`, `DEFECT`, `Rejected_qty`, `Rejn`, `contribution`, `Cumm_Contribution`) VALUES
(2, 'FR-CUT( DES. TEST)', '616', '1.25', '22.29', '22.29'),
(3, 'EXCESS FET.', '461', '0.94', '16.68', '38.98'),
(4, 'LOOSE SAND ', '250', '0.51', '9.05', '48.03'),
(5, 'TIR REJECT ', '213', '0.43', '7.71', '55.74'),
(6, 'CHIP-COLD', '175', '0.36', '6.33', '62.07'),
(7, 'COLD METAL', '169', '0.34', '6.12', '68.19'),
(8, 'GAS HOLE', '157', '0.32', '5.68', '73.87'),
(9, 'MET. REJ.', '124', '0.25', '4.49', '78.36'),
(10, 'SLAG', '117', '0.24', '4.23', '82.59'),
(11, 'GLUE', '68', '0.14', '2.46', '85.05'),
(12, 'EXTRA METAL', '68', '0.14', '2.46', '87.51'),
(13, 'PIN HOLE', '61', '0.12', '2.21', '89.72'),
(14, 'CHILL DEFECT', '50', '0.10', '1.81', '91.53'),
(15, 'MOULD LEAK', '41', '0.08', '1.48', '93.01'),
(16, 'PELTCH', '33', '0.07', '1.19', '94.21'),
(17, 'CORE BROKEN', '30', '0.06', '1.09', '95.29'),
(18, 'LUSTROUS CARBON', '30', '0.06', '1.09', '96.38'),
(19, 'OTHERS   ', '26', '0.05', '0.94', '97.32'),
(20, 'X- JOINT  ', '22', '0.04', '0.8', '98.12'),
(21, 'MOULD CRACK', '15', '0.03', '0.54', '98.66'),
(22, 'OTHERS(Sink/Crack)', '12', '0.02', '0.43', '99.1'),
(23, 'SHORT POURED', '8', '0.02', '0.29', '99.38'),
(24, 'RIPPLE', '7', '0.01', '0.25', '99.64'),
(25, 'OTHERS (SIM GAP, P/M ETC.)', '4', '0.01', '0.14', '99.78'),
(26, 'THICK FLASH', '2', '0.00', '0.07', '99.86'),
(27, 'CHIP-HOT', '2', '0.00', '0.07', '99.93'),
(28, 'DIGGING IN  ', '2', '0.00', '0.07', '100');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `access` varchar(200) NOT NULL,
  `rights` varchar(200) NOT NULL,
  `created_on` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `password`, `access`, `rights`, `created_on`) VALUES
(2, 'Admin', '123', '5', 'Admin', '2018-03-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_camshaft_scrap`
--
ALTER TABLE `tbl_camshaft_scrap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_camshaft_scrap_defects`
--
ALTER TABLE `tbl_camshaft_scrap_defects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cam_fettling_deffects`
--
ALTER TABLE `tbl_cam_fettling_deffects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cam_melting_deffects`
--
ALTER TABLE `tbl_cam_melting_deffects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cam_moulding_deffects`
--
ALTER TABLE `tbl_cam_moulding_deffects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cam_pattern_deffects`
--
ALTER TABLE `tbl_cam_pattern_deffects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_chilldepth`
--
ALTER TABLE `tbl_hardness_chilldepth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_chilldepth_b`
--
ALTER TABLE `tbl_hardness_chilldepth_b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_chilldepth_para`
--
ALTER TABLE `tbl_hardness_chilldepth_para`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_chilldepth_para_b`
--
ALTER TABLE `tbl_hardness_chilldepth_para_b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_main`
--
ALTER TABLE `tbl_hardness_main`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_observation`
--
ALTER TABLE `tbl_hardness_observation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_hardness_observation_b`
--
ALTER TABLE `tbl_hardness_observation_b`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_item_spec`
--
ALTER TABLE `tbl_item_spec`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_rejection_analysis`
--
ALTER TABLE `tbl_rejection_analysis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_camshaft_scrap`
--
ALTER TABLE `tbl_camshaft_scrap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_camshaft_scrap_defects`
--
ALTER TABLE `tbl_camshaft_scrap_defects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cam_fettling_deffects`
--
ALTER TABLE `tbl_cam_fettling_deffects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cam_melting_deffects`
--
ALTER TABLE `tbl_cam_melting_deffects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cam_moulding_deffects`
--
ALTER TABLE `tbl_cam_moulding_deffects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cam_pattern_deffects`
--
ALTER TABLE `tbl_cam_pattern_deffects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_chilldepth`
--
ALTER TABLE `tbl_hardness_chilldepth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_chilldepth_b`
--
ALTER TABLE `tbl_hardness_chilldepth_b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_chilldepth_para`
--
ALTER TABLE `tbl_hardness_chilldepth_para`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_chilldepth_para_b`
--
ALTER TABLE `tbl_hardness_chilldepth_para_b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_main`
--
ALTER TABLE `tbl_hardness_main`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_observation`
--
ALTER TABLE `tbl_hardness_observation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_hardness_observation_b`
--
ALTER TABLE `tbl_hardness_observation_b`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_item_spec`
--
ALTER TABLE `tbl_item_spec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_rejection_analysis`
--
ALTER TABLE `tbl_rejection_analysis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
