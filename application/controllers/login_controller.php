<?php //

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login_controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->view('login');
    }

    public function login() {
        $login_result = $this->login_model->login();
        if (empty($login_result)) {
            $this->session->sess_destroy();

            redirect('');
        } else {
            $session_data['user_name'] = $login_result->name;
            $session_data['login_status'] = 'TRUE';
            $this->session->set_userdata($session_data);
            redirect('add_hardness_item');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        $this->load->view('login');
    }
    
    // HARDNESS
    
    
    public function hardness_chilldept() {
        
        $data['template'] = 'hardness_chilldept';
        $data['title'] = 'Hardness & ChillDepth';
        $this->layout_admin($data);
    }
    
    public function add_hardness_item() {
        $data['item_db_data'] = $this->login_model->item_db_data();
        $data['all_heat'] = $this->login_model->get_all_heat();
        $data['template'] = 'add_hardness_item';
        $data['title'] = 'Add Item';
        $this->layout_admin($data);
    }
    public function report_hardeness() {
        $data['template'] = 'report_hardeness';
        $data['title'] = 'Hardness Report';
        $this->layout_admin($data);
    }
    public function edit_hardness_item() {
        $data['template'] = 'edit_hardness_item';
        $data['title'] = 'Edit Hardness Report';
        $this->layout_admin($data);
    }
    
    public function get_item_data1() {
        /* @var $_POST type */
        $item_name = $_POST["item_name"];
        $this->load->view('pages/get_item_data');
    }
    
    public function save_data()
    {
     $this->login_model->save_data();   
    }
    public function save_data_670()
    {
     $this->login_model->save_data_670();   
    }
    
    
    public function item_name_wise_page()
 {
      $item_name = $_POST["item_name"];
        $heat = $_POST["heat"];
        $data['heat'] = $_POST["heat"];
        $data['count'] = $_POST["count"];
        $query = "select * from tbl_hardness_main where `item`='$item_name' and `heat_no`='$heat' and `status`='0'";
        $id_data = $this->db->query($query)->row();
        if ($id_data) {
            $id = $id_data->id;
            $data['main_data'] = $id_data;
            $query1 = "select * from tbl_hardness_chilldepth where hardness_chill_id='$id'";
            $data['D'] = $this->db->query($query1)->result();

            $query2 = "select * from tbl_hardness_observation where hardness_chill_id='$id'";
            $data['D1'] = $this->db->query($query2)->result();

            $query3 = "select * from tbl_hardness_chilldepth_b where hardness_chill_id='$id'";
            $data['D2'] = $this->db->query($query3)->result();

            $query4 = "select * from tbl_hardness_observation_b where hardness_chill_id='$id'";
            $data['D3'] = $this->db->query($query4)->result();
            $this->load->view('pages/' . $item_name, $data);
        } else {
            $data['main_data'] = '';
            $data['D'] = '';
            $data['D1'] = '';
            $data['D2'] = '';
            $data['D3'] = '';
            $this->load->view('pages/' . $item_name, $data);
        }
    }
    
    
    public function get_heat_data() {
        $item = $_POST["item"];
        $query = "select heat_no from tbl_hardness_main where item='$item'";
        $result = $this->db->query($query)->result();
        $msg = '';
        if ($result) {
            $msg .= "<select name='heat_no' class='form-control select2 heat_no'>";
            $msg .= "<option value=''>Select</option>";

            foreach ($result as $row) {
                $msg .= "<option value='$row->heat_no'>$row->heat_no</option>";
            }
            $msg .= "</select>";
            echo $msg;
        }
    }

    public function get_item_data() {
        $e_date = $_POST["e_date"];
        $query = "select item from tbl_hardness_main where c_date='$e_date' group by item";
        $result = $this->db->query($query)->result();
        $msg = '';
        if ($result) {
            $msg .= "<select name='item' class='form-control select2 item'>";
            $msg .= "<option value=''>Select</option>";

            foreach ($result as $row) {
                $msg .= "<option value='$row->item'>$row->item</option>";
            }
            $msg .= "</select>";
            echo $msg;
        }
    }

    public function get_hardness_report_data() {
        $item = $_POST["item"];
        $heat_no = @$_POST["heat_no"];
        $c_date = @$_POST["c_date"];
        $msg = "";
        if (empty($item) || empty($heat_no)) {
            $query = "select * from tbl_hardness_main "
                    . " where c_date='$c_date'";

            $result = $id_data = $this->db->query($query)->result();
            $msg .= "<div class='panel-heading ui-draggable-handle'>
                                    <h3 class='panel-title'>ITEM LIST</h3>
                                </div>";
            $msg .= "<table class='table table-bordered' align='center'>";
            $msg .= "<tr><th style=' text-align:  center;'>#</th><th style=' text-align:  center;'>ITEM</th><th style=' text-align:  center;'>HEAT NO</th><th style=' text-align:  center;'>VIEW</th></tr>";
            $i = 1;
            foreach ($result as $row) {
                $msg .= "<tr>";
                $msg .= "<th style=' text-align:  center;'>$i</th>";
                $msg .= "<th style=' text-align:  center;'>$row->item</th>";
                $msg .= "<th style=' text-align:  center;'>$row->heat_no</th>";
                $msg .= "<th style='display:none;'><input type='text' class='item_id' value='$row->id'></th>";
                $msg .= "<th style=' text-align:  center;'><label class='btn btn-warning view'>View</label></th>";
                $msg .= "</tr>";
                $i++;
            }
            $msg .= "</table>";
            echo $msg;
        } else {
            $query = "select * from tbl_hardness_main "
                    . " where c_date='$c_date' and item='$item' and heat_no='$heat_no'";

            $result = $id_data = $this->db->query($query)->result();
            $msg .= "<div class='panel-heading ui-draggable-handle'>
                                    <h3 class='panel-title'>ITEM LIST</h3>
                                </div>";
            $msg .= "<table class='table table-bordered'>";
            $msg .= "<tr ><th style=' text-align:  center;'>#</th><th style=' text-align:  center;'>ITEM</th><th style=' text-align:  center;'>HEAT NO</th><th style=' text-align:  center;'>VIEW</th></tr>";
            $i = 1;
            foreach ($result as $row) {
                $msg .= "<tr>";
                $msg .= "<th style=' text-align:  center;'>$i</th>";
                $msg .= "<th style=' text-align:  center;'>$row->item</th>";
                $msg .= "<th style=' text-align:  center;'>$row->heat_no</th>";
                $msg .= "<th style='display:none;'><input type='text' class='item_id' value='$row->id'></th>";
                $msg .= "<th style=' text-align:  center;'><label class='btn btn-warning view'>View</label></th>";
                $msg .= "</tr>";
            }
            $msg .= "</table>";
            echo $msg;
        }
    }
    
    public function get_hardness_report_data1() {
        $id = $_POST["id"];
        $query = "select * from tbl_hardness_main "
                . " where `id`='$id'";

        $result['data'] = $id_data = $this->db->query($query)->row();
        $query1 = "select * from tbl_hardness_chilldepth "
                . " where hardness_chill_id='$id'";
        $result['D1'] = $this->db->query($query1)->result();

        $query2 = "select * from tbl_hardness_observation "
                . " where hardness_chill_id='$id'";

        $result['D2'] = $this->db->query($query2)->result();

        $query3 = "select * from tbl_hardness_chilldepth_para "
                . " where hardness_chill_id='$id'";

        $result['D3'] = $this->db->query($query3)->result();

        $query4 = "select * from tbl_hardness_chilldepth_b "
                . " where hardness_chill_id='$id'";

        $result['D4'] = $this->db->query($query4)->result();


        $query5 = "select * from tbl_hardness_observation_b "
                . " where hardness_chill_id='$id'";

        $result['D5'] = $this->db->query($query5)->result();

        $query6 = "select * from tbl_hardness_chilldepth_para_b "
                . " where hardness_chill_id='$id'";

        $result['D6'] = $this->db->query($query6)->result();
        $this->load->view('pages/get_hardness_report_data', $result);
    }

    public function item_validation() {
        $item = $_POST['item_name'];
        $heat = $_POST['heat'];
        $query = "select id from tbl_hardness_main where item='$item' and heat_no='$heat' and status='1'";
        $result = $this->db->query($query)->row();
        if ($result) {
            echo "1";
        }
        exit();
    }

    public function edit_hardness_form() {
        $item = $_POST["item_name"];
        $query = "select * from tbl_hardness_main "
                . " where `item`='$item'";

        $result['data'] = $id_data = $this->db->query($query)->row();
        $id = $id_data->id;

        $query1 = "select * from tbl_hardness_chilldepth "
                . " where hardness_chill_id='$id'";
        $result['data1'] = $this->db->query($query1)->result();

        $query2 = "select * from tbl_hardness_observation "
                . " where hardness_child_id='$id'";

        $result['data2'] = $this->db->query($query2)->result();
        $this->load->view('pages/edit_hardness_form', $result);
    }
    
    public function get_heat_itemwise_db()
    {
        $item = $this->input->post('item');
        $msg = '';
        $msg .= '<select class="form-control select2 enter_heat" name="enter_heat" required="">';
        $msg .= "<option value=''>Select</option>";
        $q="select heat_no from tbl_hardness_main where item='$item' and status='1'";
        $q1=$this->db->query($q)->row();
        if($q1)
        {$heat=$q1->heat_no;}
        else {$heat='';}
        $this->DB2 = $this->load->database('melt_db', TRUE);
        $query = "select heat_no from  tbl_ipcs_sheet where ipcs_item='$item' and heat_no!='$heat' group by heat_no";
        $result = $this->DB2->query($query)->result();
        if ($result) {
            foreach ($result as $row) {
                $msg .= "<option value='$row->heat_no'>$row->heat_no</option>";
            }
        }
        $msg .= '</select>';
        
        echo $msg;
        $this->DB2->close();
    }

    





    // END HARDNESS
    
    
    
    // CAMSHFAT
    public function camshaft_scrap() {
        $data['template'] = 'camshaft_scrap';
        $data['title'] = 'Camshaft Scrap';
        $this->layout_admin($data);
    }
    public function report_camshaft_scrap() {
        $data['template'] = 'report_camshaft_scrap';
        $data['title'] = 'Report';
        $this->layout_admin($data);
    }
    public function report_daily_rejection() {
        $data['template'] = 'report_daily_rejection';
        $data['title'] = 'Report Daily Rejection';
        $this->layout_admin($data);
    }
    public function camshaft_scrap_dash() {
        $data['template'] = 'camshaft_scrap_dash';
        $data['title'] = 'Dashboard';
        $this->layout_admin($data);
        
    }
    
    
    
    public function overall_dashboard() {
        echo json_encode($this->login_model->overall_dashboard());
    }

    public function save_camshaft_scrap() {
        $this->login_model->save_camshaft_scrap();
    }

    public function edit_camshaft_scrap_data() {
        $this->login_model->edit_camshaft_scrap_data();
    }

    public function get_camshaft_scrap_data() {
        $item = $_POST["item"];
        $s_date = $_POST["s_date"];
        $query = "select * from tbl_camshaft_scrap where item='$item' and c_date='$s_date'";
        $result['data'] = $id_data = $this->db->query($query)->row();
        $cam_id = $id_data->id;

        $query1 = "select * from tbl_cam_moulding_deffects where camshaft_scrap_id='$cam_id'";
        $result['D1'] = $this->db->query($query1)->result();

        $query2 = "select * from tbl_cam_melting_deffects where camshaft_scrap_id='$cam_id'";
        $result['D2'] = $this->db->query($query2)->result();

        $query3 = "select * from tbl_cam_fettling_deffects where camshaft_scrap_id='$cam_id'";
        $result['D3'] = $this->db->query($query3)->result();

        $query4 = "select * from tbl_cam_pattern_deffects where camshaft_scrap_id='$cam_id'";
        $result['D4'] = $this->db->query($query4)->result();

        $this->load->view('pages/get_camshaft_scrap_data', $result);
    }
    
    public function get_daily_rejection_data() {
        $item = $_POST["item"];
        $s_date = $_POST["s_date"];
        $query = "select * from tbl_camshaft_scrap where item='$item' and c_date='$s_date'";
        $result['data'] = $id_data = $this->db->query($query)->row();
        $cam_id = $id_data->id;
        
        $months = date("m", strtotime($s_date));
        $year = date("Y", strtotime($s_date));
        
        $query1 = "select checked.total_checked,good.total_good,rej.total_rej,fr_cut.total_fr_cut from"
        ."(select SUM(total_checked) as total_checked from tbl_camshaft_scrap where item ='$item' and MONTH(c_date)='$months' AND YEAR(c_date)='$year' ) as checked,"
        ."(select SUM(total_good) as total_good from tbl_camshaft_scrap where item ='$item' and MONTH(c_date)='$months' AND YEAR(c_date)='$year' ) as good,"
        ."(select SUM(total_rej) as total_rej from tbl_camshaft_scrap where item ='$item' and MONTH(c_date)='$months' AND YEAR(c_date)='$year' ) as rej,"
        ."(select SUM(fr_cut) as total_fr_cut from tbl_camshaft_scrap where item ='$item' and MONTH(c_date)='$months' AND YEAR(c_date)='$year' ) as fr_cut";
        $result['total_all'] = $id_data = $this->db->query($query1)->row();
        
        // MOULD
        $mould_q1 = "select defect_name,defects_sum from tbl_cam_moulding_deffects where camshaft_scrap_id='$cam_id'";
        $result['day_mould'] = $this->db->query($mould_q1)->result();
        $mould_q2="select SUM(mould.defects_sum) as total_sum_mould,mould.defect_name from tbl_camshaft_scrap cs "
        . "LEFT JOIN tbl_cam_moulding_deffects mould ON cs.id=mould.camshaft_scrap_id where cs.item='$item' "
        . "and MONTH(cs.c_date)='$months' AND YEAR(cs.c_date)='$year' group by mould.defect_name "
        . "order by mould.id ASC";
        $result['cumm_mould'] = $this->db->query($mould_q2)->result();
        //END MOULD
        
        // MELT
        $melt_q1 = "select defect_name,defects_sum from tbl_cam_melting_deffects where camshaft_scrap_id='$cam_id'";
        $result['day_melt'] = $this->db->query($melt_q1)->result();
        $melt_q2="select SUM(mould.defects_sum) as total_sum_mould,mould.defect_name from tbl_camshaft_scrap cs "
        . "LEFT JOIN tbl_cam_melting_deffects mould ON cs.id=mould.camshaft_scrap_id where cs.item='$item' "
        . "and MONTH(cs.c_date)='$months' AND YEAR(cs.c_date)='$year' group by mould.defect_name "
        . "order by mould.id ASC";
        $result['cumm_melt'] = $this->db->query($melt_q2)->result();
        //END MELT
        
        // FELT
        $felt_q1 = "select defect_name,defects_sum from tbl_cam_fettling_deffects where camshaft_scrap_id='$cam_id'";
        $result['day_felt'] = $this->db->query($felt_q1)->result();
        $felt_q2="select SUM(mould.defects_sum) as total_sum_mould,mould.defect_name from tbl_camshaft_scrap cs "
        . "LEFT JOIN tbl_cam_fettling_deffects mould ON cs.id=mould.camshaft_scrap_id where cs.item='$item' "
        . "and MONTH(cs.c_date)='$months' AND YEAR(cs.c_date)='$year' group by mould.defect_name "
        . "order by mould.id ASC";
        $result['cumm_felt'] = $this->db->query($felt_q2)->result();
        //END FELT
        
        // PATTERN
        $pattern_q1 = "select defect_name,defects_sum from tbl_cam_pattern_deffects where camshaft_scrap_id='$cam_id'";
        $result['day_pattern'] = $this->db->query($pattern_q1)->result();
        $pattern_q2="select SUM(mould.defects_sum) as total_sum_mould,mould.defect_name from tbl_camshaft_scrap cs "
        . "LEFT JOIN tbl_cam_pattern_deffects mould ON cs.id=mould.camshaft_scrap_id where cs.item='$item' "
        . "and MONTH(cs.c_date)='$months' AND YEAR(cs.c_date)='$year' group by mould.defect_name "
        . "order by mould.id ASC";
        $result['cumm_pattern'] = $this->db->query($pattern_q2)->result();
        //END PATTERN
        
        
        $this->load->view('pages/get_daily_rejection_data', $result);
    }

    public function get_part_no() {
        $s_date = $_POST["s_date"];
        if ($s_date) {
            $condition = "where c_date ='$s_date'";
        }
        $query = "select item from tbl_camshaft_scrap  $condition";

        $result = $this->db->query($query)->result();
        $msg = '';
        if ($result) {
            $msg .= "<div class='col-md-6'>                                        
                    <select class='form-control select2 item'>
                        <option value=''>Select</option>";
            foreach ($result as $row) {
                $msg .= "<option value='$row->item'>$row->item</option>";
            }
            $msg .= "</select>
                </div>";
            echo $msg;
        }
    }

    
    public function add_camshaft_scrap() {

        $data['template'] = 'add_camshaft_scrap';
        $data['title'] = 'Add Camshaft Scrap';
        $this->layout_admin($data);
    }

    public function edit_camshaft_scrap() {

        $data['template'] = 'edit_camshaft_scrap';
        $data['title'] = 'EDIT Camshaft Scrap';
        $this->layout_admin($data);
    }

    public function camshaft_scrap_itemwise_page() {
        $item_name = $_POST["item_name"];
        $c_date = $_POST["c_date"];
        $data['c_date'] = $_POST["c_date"];
        $data['count'] = $_POST["count"];
        $data['item_name'] = $_POST["item_name"];

        $query = "select * from tbl_camshaft_scrap where `item`='$item_name' and `c_date`='$c_date'";

        $id_data = $this->db->query($query)->row();
        if ($id_data) {
            $id = $id_data->id;
            $data['main_data'] = $id_data;
            $query1 = "select * from tbl_cam_moulding_deffects where camshaft_scrap_id='$id'";
            $data['MOULD'] = $this->db->query($query1)->result();

            $query2 = "select * from tbl_cam_melting_deffects where camshaft_scrap_id='$id'";
            $data['MELT'] = $this->db->query($query2)->result();

            $query3 = "select * from tbl_cam_fettling_deffects where camshaft_scrap_id='$id'";
            $data['FELT'] = $this->db->query($query3)->result();

            $query4 = "select * from tbl_cam_pattern_deffects where camshaft_scrap_id='$id'";
            $data['PATTERN'] = $this->db->query($query4)->result();
            $this->load->view('pages/edit_camshaft_scrap', $data);
        } else {
            $data['main_data'] = '';
            $data['MOULD'] = '';
            $data['MELT'] = '';
            $data['FELT'] = '';
            $data['PATTERN'] = '';
            $this->load->view('pages/camshaft_scrap', $data);
        }
    }

    public function item_validation_camshaft_scrap() {
        $item = $_POST['item_name'];
        $c_date = $_POST['c_date'];
        $query = "select id from tbl_camshaft_scrap where item='$item' and c_date='$c_date' and status='1'";
        $result = $this->db->query($query)->row();
        if ($result) {
            echo "1";
        }
        exit();
    }
    
    public function report_pareto_analysis()
    {
      $data['template'] = 'report_pareto_analysis';
        $data['title'] = 'Report Pareto Analysis';
        $this->layout_admin($data);   
    }
    
    public function get_pareto_analysis_data()
    {
    $query="select * from tbl_rejection_analysis where id >='2' and id<='9'";
    $data['pareto_data']=$this->db->query($query)->result();
    $query1="select * from tbl_rejection_analysis";
    $data['pareto_data1']=$this->db->query($query1)->result();
    $this->load->view('pages/get_pareto_analysis_data', $data);
    }

    // END

    
    

}
