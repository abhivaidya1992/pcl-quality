<?php

//

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Emailquality_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function save_file_upload() {
         $filename = $_FILES["userfile"]["tmp_name"];
        if ($_FILES["userfile"]["size"] > 0) {
            $file = fopen($filename, "r");
            fgetcsv($file);
            while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE) {
                
                $data = array(
                    'ticket' => $emapData[0],
                    'ticket_type' => $emapData[1],
                    'invoice_date' => $emapData[2],
                    'invoice_no' => $emapData[3],
                    'cust_name' => $emapData[4],
                    'mobile' => $emapData[5],
                    'franchisee_name' => $emapData[6],
                    'franchisee_code' => $emapData[7],
                    'license_plate_no' => $emapData[8],
                    'team' => $emapData[9],
                    'cluster' => $emapData[10],
                    'zone' => $emapData[11],
                    'fofo_coco' => $emapData[12],
                    'area' => $emapData[13],
                );

                $insertId = $this->email_model->add_file_db($data);
            }
            fclose($file);
            redirect('file_upload');
        }
    }
    
    
    public function search_ticket()
    {
     $search=$_POST["search"] ;
     $query="SELECT * FROM tbl_file_upload where ticket='$search'";
     $result= $this->db->query($query)->row();
     $data['search_data']=$result;
     $this->load->view('pages/search_ticket_data',$data);
        
        
    }
    
    public function save_data_form()
    {
     $this->email_model->save_data_form();   
    }

    
   

}
