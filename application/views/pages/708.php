<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/datetimepicker-master/jquery.datetimepicker.css"/>                      
<script type="text/javascript" src="<?php echo base_url();?>js/datetimepicker-master/jquery.datetimepicker.css"></script>
<link rel="stylesheet" href="css/style.css">
<form class="form-horizontal" action="#" id="708_<?php echo $count; ?>">
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Hardness Chilldepth  - 708 - <?php echo $heat; ?></strong> </h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control item" name="item" required="" value="708" readonly="" style=" display: none;">
                                <input type="text" class=" form-control" name="heat_no" required="" id="heat_no" value="<?php echo $heat; ?>" style=" display: none;">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Part No</label>
                                        <div class="col-md-6">                                        
                                            <input type="text" name="part_no" class="form-control part_no" value="12711 - 69LBO" readonly="" style="color:#000;">
                                        </div>
                                    </div> 
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Date</label>
                                        <div class="col-md-6">
                                            <?php
                                            if(@$main_data->c_date)
                                            {?>
                                         <input type="text" class=" form-control datetimepicker2 c_date" name="c_date" required="" id="d1" value="<?php echo @$main_data->c_date;?>">
                                            <?php }else{
                                                $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
                                                $date1 = $date->format('Y-m-d');
                                                ?>
                                            <input type="text" class=" form-control datetimepicker2 c_date" name="c_date" required="" id="d1" value="<?php echo $date1;?>">    
                                            <?php }?>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div id="item_img"><img src="Audio/708_img.png" class=" " width="900"></div>
<!--                        <div id="item_img" ><img src="Audio/708_cam.png" width="500" class=" "></div>-->
                    </div>
                </div>
            </div>
        </div>                    
        <div class="row">
            <div class="col-md-12">

                <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default" style="overflow-x:auto;overflow-y:auto;border: 0px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">OBSERVATIONS</h3>
                    </div>
                    <div class="panel-body">
                        <label class=" btn btn-primary addmore_708_<?php echo json_decode($count); ?>">L <i class="fa fa-plus"></i></label>
                        <br><br><br>
                        <table class="table table-bordered table_708_<?php echo $count; ?>" >
                            <tbody id="append_data_708_<?php echo $count; ?>" class="append_data_708_<?php echo $count; ?>">
                                <tr>
                                    <th>AT POINT</th>
                                    <th></th>
                                    <td >''a'</td>
                                    <td >''b'</td>
                                    <td >''c'</td>
                                    <td >''d'</td>
                                    <td>''e'</td>
                                    <td> Cam Center  HRB</td>
                                </tr>
                            <input type="text" name="at_point[]" value="a" style=" display: none;">
                            <input type="text" name="at_point[]" value="b" style=" display: none;">
                            <input type="text" name="at_point[]" value="c" style=" display: none;">
                            <input type="text" name="at_point[]" value="d" style=" display: none;">
                            <input type="text" name="at_point[]" value="e" style=" display: none;">
                            <input type="text" name="at_point[]" value="Cam Center  HRB" style=" display: none;">
                            <tr>
                                <th>CUSTOMER SPEC.</th>
                                <th>MM - HRc</th>
                                <td>2 – 25 Min.</td>
                                <td>2 – 45 Min.</td>
                                <td>2 – 45 Min.</td>
                                <td>2 – 45 Min.</td>
                                <td>2 – 25 Min.</td>
                                <td>90-107 </td>
                            </tr>
                            <input type="text" name="cust_spec[]" value="2 – 25 Min." style=" display: none;">
                            <input type="text" name="cust_spec[]" value="2 – 45 Min." style=" display: none;">
                            <input type="text" name="cust_spec[]" value="2 – 45 Min." style=" display: none;">
                            <input type="text" name="cust_spec[]" value="2 – 45 Min." style=" display: none;">
                            <input type="text" name="cust_spec[]" value="2 – 25 Min." style=" display: none;">
                            <input type="text" name="cust_spec[]" value="90-107" style=" display: none;">
                            <tr>
                                <th>WORKING SPEC.</th>
                                <th >MM - HRc</th>
                                <td>3 – 25 Min.</td>
                                <td>3 – 45 Min.</td>
                                <td>3 – 45 Min.</td>
                                <td>3 – 45 Min.</td>
                                <td>3 – 25 Min.</td>
                                <td>92-105 </td>
                            </tr>
                            <input type="text" name="working_spec[]" value="3 – 25 Min." style=" display: none;">
                            <input type="text" name="working_spec[]" value="3 – 45 Min." style=" display: none;">
                            <input type="text" name="working_spec[]" value="3 – 45 Min." style=" display: none;">
                            <input type="text" name="working_spec[]" value="3 – 45 Min." style=" display: none;">
                            <input type="text" name="working_spec[]" value="3 – 25 Min." style=" display: none;">
                            <input type="text" name="working_spec[]" value="92-105" style=" display: none;">

                            <?php
                            if ($main_data) {
                                $a1 = '';
                                $a3 = '';
                                foreach ($D as $row) {
                                    $a1[] = explode(",", $row->data_value);
                                }
                                $g = 0;
                                $L = '';
                                $L_C = '';
                                $L_C_l = '';
                                $a = '';
                                foreach ($D1 as $row) {
                                    $L_C[] = $row->L_C;
                                    $L_C_l .= $row->L_C . ",";
                                    $L[] = $row->L;
                                }
                                $L_C_l = explode(",", rtrim($L_C_l, ","));
                                for ($k = 0; $k < count($L); $k++) {
                                    $L_C1 = explode(",", $L_C[$k]);
                                    ?>
                                    <tr>
                                        <td><input type='text'  class="form-control L1_708_<?php echo json_decode($count); ?>" name="L[]" value="<?php echo $L[$k]; ?>"><input  class="btn btn-info btn-rounded add_c_<?php echo json_decode($count); ?>"  value="C-<?php echo $k; ?>"></td></tr>
                                    <?php
                                    for ($i = 0; $i < count($L_C1); $i++) {
                                        ?>
                                        <tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control a_count_708_<?php echo json_decode($count); ?>" name="L<?php echo $k; ?>[]" value="<?php echo $L_C1[$i]; ?>"></td>
                                            <?php
                                            for ($j = 0; $j < count($a1[$g]); $j++) {
                                                ?>
                                                <td><input type="text" style="width:60px;" class="form-control" name="a<?php echo $g; ?>[]" value="<?php print_r($a1[$g][$j]); ?>"></td>         
                                            <?php } ?></tr>    
                                            <?php
                                        $g++;
                                    }
                                }
                            }
                            if ($main_data) {
                                ?>
                                <input type="text" class=" form-control" name="hadness_id" id="hadness_id" value="<?php echo $main_data->id; ?>" style=" display: none;">    
                            <?php } else { ?>
                                <input type="text" class=" form-control" name="hadness_id" id="hadness_id" value="" style=" display: none;">    
                            <?php } ?>

                            </tbody>
                        </table>

                        <!--------------------B----------------------------------------->
                        <label class=" btn btn-danger addmore_b_708_<?php echo json_decode($count); ?>">B <i class="fa fa-plus"></i></label>
                        <br><br><br>
                        <table class="table table-bordered table_b_708_<?php echo $count; ?>" >
                            <tbody id="append_data_b_708_<?php echo $count; ?>" class="append_data_b_708_<?php echo $count; ?>">
                                <tr>
                                    <th>AT POINT</th>
                                    <th></th>
                                    <td colspan="8">ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface</td>
                                </tr>
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="ROCKWELL HARDNESS-B Scale( HRB) At 2 mm from Cast Surface" style=" display: none;">


                            <tr>
                                <th>CUSTOMER SPEC.</th>
                                <th>MM - HRc</th>
                                <td>Front End 90 - 107</td>
                                <td>BRG 1 90-107</td>
                                <td>BRG.2 90 - 107</td>
                                <td>BRG.3 90 - 107</td>
                                <td>BRG.4 90 - 107</td>
                                <td>BRG.5 90 - 107</td>
                                <td>BRG.6 90 - 107</td>
                                <td>Collar 1 90 - 107</td>
                            </tr>
                            <input type="text" name="cust_spec_b[]" value="Front End 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG 1 90-107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG.2 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG.3 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG.4 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG.5 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG.6 90 - 107" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="Collar 1 90 - 107" style=" display: none;">
                            <tr>
                                <th>WORKING SPEC.</th>
                                <th >MM - HRc</th>
                                <td>Front End 92 - 105</td>
                                <td>BRG 1 92-105</td>
                                <td>BRG.2 92 - 105</td>
                                <td>BRG.3 92 - 105</td>
                                <td>BRG.4 92 - 105</td>
                                <td>BRG.5 92 - 105</td>
                                <td>BRG.6 92 - 105</td>
                                <td>Collar 1 92 - 105</td>
                            </tr>
                            <input type="text" name="working_spec_b[]" value="Front End 92 - 105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG 1 92-105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG.2 92 - 1075" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG.3 92 - 105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG.4 92 - 105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG.5 92 - 105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG.6 92 - 105" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="Collar 1 92 - 105" style=" display: none;">

                            <?php
                            if ($D2) {
                                $a1 = '';
                                $a3 = '';
                                foreach ($D2 as $row) {
                                    $a1[] = explode(",", $row->data_value);
                                }
                                $g = 0;
                                $L = '';
                                $L_C = '';
                                $L_C_l = '';
                                $a = '';
                                foreach ($D3 as $row) {
                                    $L_C[] = $row->L_C;
                                    $L_C_l .= $row->L_C . ",";
                                    $L[] = $row->L;
                                }
                                $L_C_l = explode(",", rtrim($L_C_l, ","));
                                for ($k = 0; $k < count($L); $k++) {
                                    $L_C1 = explode(",", $L_C[$k]);
                                    ?>
                                    <tr>
                                        <td><input type='text'  class="form-control L1_b_708_<?php echo json_decode($count); ?>" name="LB[]" value="<?php echo $L[$k]; ?>"><input  class="btn btn-info btn-rounded add_b_c_<?php echo json_decode($count); ?>"  value="B-<?php echo $k; ?>"></td></tr>
                                    <?php
                                    for ($i = 0; $i < count($L_C1); $i++) {
                                        ?>
                                        <tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control b_count_708_<?php echo json_decode($count); ?>" name="LB<?php echo $k; ?>[]" value="<?php echo $L_C1[$i]; ?>"></td>
                                            <?php
                                            for ($j = 0; $j < count($a1[$g]); $j++) {
                                                ?>
                                                <td><input type="text" style="width:90px;" class="form-control" name="b<?php echo $g; ?>[]" value="<?php print_r($a1[$g][$j]); ?>"></td>         
                                            <?php } ?></tr>    
                                            <?php
                                        $g++;
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <!--------------------/B----------------------------------------->
                        <table class="table table-bordered">
                            <tr>
                                <th><div id="item_img" ><img src="Audio/708_cam.png" width="500" class=" "></div></th>
                                <th><textarea class=" form-control" name="remark" placeholder="REMARK"><?php echo @$main_data->remark; ?></textarea></th>
                            </tr>
                        </table>
                        
                        <table class="table table-bordered">
                            <tr>
                                <td>Checked By</td>
                                <td><input type="text" class="form-control approved_by " name="checked_by" value="<?php echo @$main_data->checked_by; ?>"></td>
                            </tr>
                            <tr>
                                <td >Approved By</td>
                                <td><input type="text" class="form-control approved_by " name="approved_by" value="<?php echo @$main_data->approved_by; ?>"></td>
                            </tr>
                        </table>
                        <div class="panel-footer">
                            <label class="btn btn-primary pull-right submit" id="save" name="print_option" value="" onclick="submit('708_<?php echo $count; ?>');">save</label>
                            <label class="btn btn-primary pull-left submit" id="submit" name="print_option" value="" onclick="submit('708_<?php echo $count; ?>');">Submit</label>
                            <input type="text" class="status" name="status" style=" display: none;">
                        </div> 
                    </div>
                </div>
            </div>    
        </div>
    </div>
</form>
<div id="loader_708_<?php echo $count;?>" style="  margin-left: 500px; margin-top: 800px; position: absolute; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>
<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
var all=<?php echo json_decode($count); ?>;
var all1=<?php echo json_decode($count); ?>;
all="708_" + all;
window['i_708_'+<?php echo json_decode($count); ?>] = 0; 
window['j_708_'+<?php echo json_decode($count); ?>] = -1; 
window['k_708_'+<?php echo json_decode($count); ?>] = 0; 
var k_708= 0;
var add_li = <?php echo json_decode($count); ?>;
var item_count=$("#item_count").val();
add_li= "add_l" + item_count;
var add_more;
add_more= <?php echo json_decode($count); ?>;
add_more= "addMore708_" + add_more; 
$(".addmore_708_<?php echo json_decode($count); ?>").on('click', function () {
window["addMore708_" + <?php echo json_decode($count); ?>]();
window["add_l708_" + <?php echo json_decode($count); ?>]();
});
//
window["addMore708_" + <?php echo json_decode($count); ?>] = function() {
$.ajax({
url: "",
cache: false,
async: false,
success: function (data) {
    var l_length=0;
    l_length = document.getElementsByClassName('L1_708_'+<?php echo json_decode($count); ?>);
   var l_length_count=parseInt(l_length.length);
var data = "<tr><td><input type='text' placeholder='L-"+ l_length_count+"' class='form-control L1_708_"+<?php echo json_decode($count); ?>+"' name='L[]' ><input  class='btn btn-info btn-rounded add_c_"+<?php echo json_decode($count); ?>+" "+<?php echo json_decode($count); ?>+"_708_show_l" + window['i_708_'+<?php echo json_decode($count); ?>] + "' value='C-"+l_length_count+"' id='708_"+<?php echo json_decode($count); ?>+"_show_l" + window['i_708_'+<?php echo json_decode($count); ?>] + "'></td></tr>";
$('#append_data_708_'+<?php echo json_decode($count); ?>).append(data);
}
});
window['i_708_'+<?php echo json_decode($count); ?>]++;   
window['j_708_'+<?php echo json_decode($count); ?>]++;   
}

//

$(document).on('click', '.add_c_'+<?php echo json_decode($count); ?>, function (e) {
    var id_this=$(this).val();
    id_this=id_this.split("-");
    id_this=id_this[1];
    
    var cost=0;
    cost = document.getElementsByClassName('a_count_708_'+<?php echo json_decode($count); ?>);
   var cost_count=parseInt(cost.length);
$('.table_708_'+<?php echo json_decode($count); ?>+' tbody ').append('<tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control a_count_708_'+<?php echo json_decode($count); ?>+'" name="L'+id_this+'[]" placeholder="L'+window['j_708_'+<?php echo json_decode($count); ?>]+'-C-'+window['k_708_'+<?php echo json_decode($count); ?>]+'"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]"></td><td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]">\n\
    </td><td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]"></td>\n\
<tr>');
    window['k_708_'+<?php echo json_decode($count); ?>]++; 
    });

    //
//    window["add_l708_" + <?php echo json_decode($count); ?>] = function() 
//    {
//    var cost=0;
//    cost = document.getElementsByClassName('L1_708_'+<?php echo json_decode($count); ?>);
//    var show_l=0;
//    show_l=parseInt(cost.length);
//    show_l=parseInt(show_l) - parseInt(2) ;
//    show_l="708_"+<?php echo json_decode($count); ?>+"_show_l" +show_l;
//    document.getElementById(show_l).disabled = true;
//    //if(parseInt(cost.length)==1)
//    //{
//    //cost=1;    
//    //}
//    //else
//    //{
//    //cost=parseInt(cost.length);    
//    //}
//    //
//    }

    //

    $(document).on('click', '.submit', function (e) {
        if($(this).attr('id')=="submit")
        {$('.status').val("1"); }
        else{$('.status').val("0");}
var form = document.getElementById("708_<?php echo $count;?>");
for(var i=0; i < form.elements.length; i++){
if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
swal(
'Oops...',
'ALL Fields are Mandatory',
'error'
);
$('input[name='+form.elements[i].name+']').focus();
$('select[name='+form.elements[i].name+']').focus();
return;
}
}    
    $("#loader_708_"+<?php echo json_decode($count); ?>).show();
    $("#loader_708_"+<?php echo json_decode($count); ?>).fadeOut(5000);
    
    $.ajax({
    url:'<?php echo base_url(); ?>login_controller/save_data',
    type: 'POST',
    data: $("#708_"+<?php echo $count; ?>).serialize(),
    success: function(data){
$("#hadness_id").val(data);
    swal({
  position: 'top-end',
  type: 'success',
  title: 'Data Inserted',
  showConfirmButton: false,
  timer: 1500
});  
var he=$("#heat_no").val();
//$("#real_708_"+<?php echo json_decode($count); ?>).remove();
//$('#708_'+he+'_item').remove();
    }
    });
    });

// ------------------B--------------------------

window['k_b_708_'+<?php echo json_decode($count); ?>] = 0;
window['i_b_708_'+<?php echo json_decode($count); ?>] = 0;     
$(".addmore_b_708_<?php echo json_decode($count); ?>").on('click', function () {
window["addMore_b_708_" + <?php echo json_decode($count); ?>]();
});  

window["addMore_b_708_" + <?php echo json_decode($count); ?>] = function() {
$.ajax({
url: "",
cache: false,
async: false,
success: function (data) {
    var b_length=0;
    b_length = document.getElementsByClassName('L1_b_708_'+<?php echo json_decode($count); ?>);
   var b_length_count=parseInt(b_length.length);
var data = "<tr><td><input type='text' placeholder='L-"+ b_length_count+"' class='form-control L1_b_708_"+<?php echo json_decode($count); ?>+"' name='LB[]' ><input  class='btn btn-info btn-rounded add_b_c_"+<?php echo json_decode($count); ?>+"' value='B-"+b_length_count+"' '></td></tr>";
$('#append_data_b_708_'+<?php echo json_decode($count); ?>).append(data);
}
});
window['i_b_708_'+<?php echo json_decode($count); ?>]++;   
}

$(document).on('click', '.add_b_c_'+<?php echo json_decode($count); ?>, function (e) {
    var id_this=$(this).val();
    id_this=id_this.split("-");
    id_this=id_this[1];
    var cost_b=0;
    cost_b = document.getElementsByClassName('b_count_708_'+<?php echo json_decode($count); ?>);
   var cost_count_b=parseInt(cost_b.length);
$('.table_b_708_'+<?php echo json_decode($count); ?>+' tbody ').append('<tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control b_count_708_'+<?php echo json_decode($count); ?>+'" name="LB'+id_this+'[]" placeholder="L'+window['i_b_708_'+<?php echo json_decode($count); ?>]+'-C-'+window['k_b_708_'+<?php echo json_decode($count); ?>]+'"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td><td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="b'+cost_count_b+'[]">\n\
    </td><td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:90px;" class="form-control" name="b'+cost_count_b+'[]"></td>\n\
<tr>');
    window['k_b_708_'+<?php echo json_decode($count); ?>]++; 
    });
    
   // ---------------------/B---------------------------------------------------------------- 
    </script>
<script src='js/Sortable.js'></script>
<script>
    $(document).ready(function () {
    $(document).on('click', '.recipe-table__del-row-btn', function (e) {
    var $el = $(e.currentTarget);
    var $row = $el.closest('tr');
    $row.remove();
    return false;
    });
    Sortable.create(
    $('.append_data_708_<?php echo $count; ?>')[0],
    {
    animation: 150,
    scroll: true,
    handle: '.drag-handler',
    onEnd: function () {
    }
    }
    );
    Sortable.create(
    $('.append_data_b_708_<?php echo $count; ?>')[0],
    {
    animation: 150,
    scroll: true,
    handle: '.drag-handler',
    onEnd: function () {
    }
    }
    );
    });
    
    
    
    
    
     $(document).on('keydown', 'input', function (e) {
 if (e.which == 13  || e.keyCode == 13  ) 
 {      e.preventDefault();
        var $canfocus=$(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
}   
});
 </script>  
<script src="<?php echo base_url();?>js/datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
 <script src="<?php echo base_url();?>js/datetimepicker-master/build/my.js"></script>
        
   
    

