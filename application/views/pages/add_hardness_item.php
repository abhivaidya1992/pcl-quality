<script type="text/javascript" src="<?php echo base_url();?>js/sweetalert-dev.js"></script>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url();?>css/sweetalert.css"/>
<style>

    body {
        
    }

    input[type="text"]:focus {
        border-color: #66afe9;
        outline: 0;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, 0.6);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, 0.6);
    }


             
    div.tableContainer {
        clear: both;
        border: 1px solid #963;
        height: 285px;
        overflow: auto;
        width: 100%;
    }

     
    html>body div.tableContainer {
        overflow: hidden;
        width: 100%;
    }

                  
    div.tableContainer table {
        float: left;
        width: 740px 
    }


    html>body div.tableContainer table {
         width: 756px 
    }


    thead.fixedHeader tr {
        position: relative;
    }

    thead.fixedHeader th {
        background: #C96;
        border-left: 1px solid #EB8;
        border-right: 1px solid #B74;
        border-top: 1px solid #EB8;
        font-weight: normal;
        padding: 4px 3px;
        text-align: left
    }

    html>body tbody.scrollContent {
        display: block;
        height: 262px;
        overflow: auto;
        width: 100%
    }

    html>body thead.fixedHeader {
        display: table;
        overflow: auto;
        width: 100%;
        
    }

    tbody.scrollContent td, tbody.scrollContent tr.normalRow td {
        background: #FFF;
        border-bottom: 1px solid #CCC;
       
        border-right: 1px solid #CCC;
        border-top: 1px solid #DDD;
        padding: 2px 3px 3px 4px
    }
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

</style>
<body>  
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Hardness & Chilldepth</a></li>
</ul>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12" style=" background-color: #F0F8FF;">
            <form class="form-horizontal"  method="post" name="myForm1" id="myForm1" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Add Item</strong> </h3>

                    </div>
                     
                    <div class="panel-body" style=" background-color: #ddd;">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Add Item</label>
                                        <div class="col-md-6">                                        
                                            <select class="form-control select2 item" name="item" required="">
                                                <option value="">Select</option>
                                                <?php
                                                foreach($item_db_data as $row)
                                                {?>
                                                <option value="<?php echo $row->ipcs_item;?>"><?php echo $row->ipcs_item;?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div> 
                                </div>
                            </div>     
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Enter Heat No</label>
                                        <div class="col-md-6">                                        
<!--                                            <input class=" form-control enter_heat" type="text">-->
<!--                                            <input list="browsers" class=" form-control enter_heat" type="text">
  <datalist id="browsers">
    <?php //foreach ($all_heat as $row)
    //{?>
      <option value="<?php //echo $row->heat_no;?>">
    <?php //} ?>
  </datalist>-->
                                            <div id="heat_append"></div>
                                        </div>
                                        <label class="btn btn-primary pull-right" id="s1" name="print_option" value="">Submit</label>

                                    </div> 
                                </div>
                            </div>     
                        </div>
                        <div id="add_item">
                        </div>
                    </div>
            </form>
        </div>
        <table class="table table-bordered"><tbody><tr id="append_item"></tr></tbody></table>
        <!--        <div id="show_item_data1"></div> -->
        <div id="show_item_data"></div> 
    </div> 
</div>
</div


<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
j=0;
var glo='';
$(document).on('click', '#s1', function (e) {
var item_name = $('.item').val();
var heat = $('.enter_heat').val();
if(item_name=='' || heat=='')
{
swal({
  position: 'top-end',
  type: 'warning',
  title: 'All Fields are Mandatory',
  showConfirmButton: false,
  timer: 1500
}); 
return;
}
var con_item_heat=item_name + "_" + heat;
// ITEM ALREADY EXIST IN DB 
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/item_validation'); ?>",
data: {item_name: item_name,heat:heat},
cache: false,
async: false,
success: function (data) {
if(data!='' || data!=null){
glo=data;}
else{
glo='';}
}
}); 
if(glo)
{
swal({
  position: 'top-end',
  type: 'warning',
  title: 'Item Already Exist',
  showConfirmButton: false,
  timer: 1500
}); 
return;}
// ITEM ALREADY EXIST IN DB 
var con_item_heat1=item_name + "_" + heat + "_" + j;
var con_item_heat2=item_name + "_"+ j;

var cost=0;
cost = document.getElementsByClassName('item_click');

for(var i=0;i<cost.length;i++)
{
    
 if(cost[i].value.substring(0, cost[i].value.length-2)==con_item_heat)
 {
swal({
  position: 'top-end',
  type: 'warning',
  title: 'Item Already Exist',
  showConfirmButton: false,
  timer: 1500
}); 
return;   
}
}

$.ajax({
url: "",
success: function (data) {
var data = '';
data += '<th id="real_'+ con_item_heat2 +'"><button class="btn btn-danger item_click"  name="" value=' + con_item_heat1 + ' >' + con_item_heat + '</button> </th>';
var data1='<div id="' + con_item_heat + '_item" class="sss'+j+' ii"></div><input type="text" class="' + con_item_heat + '_item" style="display:none;">';

$('#append_item').append(data);
$('#show_item_data').append(data1);
j++;
}
});
});
$(document).on('click', '.item_click', function (e) {

//$(this).css('background-color','black');
var name = $(this).val();
var item_name=name.split("_");
var real_item=item_name[0];
var heat=item_name[1];
var count=item_name[2];
var abc=real_item + "_" + heat;
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/item_name_wise_page'); ?>",
data: {item_name: real_item,heat:heat,count:count},
cache: false,
async: false,
success: function (data) {
var aa=$('.'+ abc +'_item').val();
if(aa)
{
var cost=0;
cost = document.getElementsByClassName('ii');
var show_l=0;
show_l=parseInt(cost.length);
for(var i=0;i<show_l;i++)
{
  if(i!=count)
    {
    $('.sss'+ i).hide();   
    }
}
$('.sss'+ count).show();   


}
    else
    {
    var cost=0;
    cost = document.getElementsByClassName('ii');
    var show_l=0;
    show_l=parseInt(cost.length);
    for(var i=0;i<show_l;i++)
        {
        if(i!=count)
        {
        $('.sss'+ i).hide();   
        }
        }
        $('#'+ abc +'_item').show();        
        $('#'+ abc +'_item').html(data);
        $('.'+ abc +'_item').val("1");
        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
       
        }




        // $('#'+ name +'_item').css({top: 500, left: 200, position:'absolute'});
        // 670
        //    if(item=='' && item_name=="670")
        //    {
        //    $('#670_item').show();     
        //    $('#670_item').html(data);
        //    $('.' + item_name +'_item').val("1");
        //    $('#708_item').hide();
        //    $('#709_item').hide();
        //    }
        //    else if(item=="1" && item_name=="670")
        //    {
        //    $('#670_item').show();   
        //    $('#708_item').hide();
        //    $('#709_item').hide();
        //    }
        //    // END 670
        //    // 708
        //    else if(item=='' && item_name=="708")
        //    {
        //     $('#708_item').show(); 
        //    $('#708_item').html(data);
        //    $('.' + item_name +'_item').val("1");
        //    $('#670_item').hide();
        //    $('#709_item').hide();
        //    }
        //    else if(item=="1" && item_name=="708")
        //    {
        //    $('#708_item').show();   
        //    $('#670_item').hide();
        //    $('#709_item').hide();
        //    }
        //    // END 708
        //    // 709
        //    else if(item=='' && item_name=="709")
        //    {
        //     $('#709_item').show(); 
        //    $('#709_item').html(data);
        //    $('.' + item_name +'_item').val("1");
        //    $('#670_item').hide();
        //    $('#708_item').hide();
        //    }
        //    else if(item=="1" && item_name=="709")
        //    {
        //    $('#709_item').show();   
        //    $('#670_item').hide();
        //    $('#708_item').hide();
        //    }
        // END 709
        }
        });
        });





        function isNumber(e) {
        var charCode;
        if (e.keyCode > 0) {
        charCode = e.which || e.keyCode;
        }
        else if (typeof (e.charCode) != "undefined") {
        charCode = e.which || e.keyCode;
        }
        if (charCode == 46)
        return true
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
        return true;
        }


//$(document).ready(function(){
// $(document).bind("contextmenu",function(e){
//   return false;
// });
//});



$(document).on('change', '.item', function (e) {

//$(this).css('background-color','black');
var item = $(this).val();

$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_heat_itemwise_db'); ?>",
data: {item: item},
cache: false,
async: false,
success: function (data) {
    $('#heat_append').html(data);
    $('.select2').select2();
}
});
});
</script>

<script src='js/Sortable.js'></script>

<script type="text/javascript" src="<?php echo base_url();?>js/select_2.js"></script>
</body>