<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>js/datetimepicker-master/jquery.datetimepicker.css"/>                      
<script type="text/javascript" src="<?php echo base_url();?>js/datetimepicker-master/jquery.datetimepicker.css"></script>
<link rel="stylesheet" href="css/style.css">
<form class="form-horizontal" action="#" id="670_<?php echo $count; ?>">
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Hardness Chilldepth  - 670 - <?php echo $heat; ?> </strong> </h3>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                                <input class="form-control item" name="item" required="" value="670" readonly="" style=" display: none;">
                                <input type="text" class=" form-control" name="heat_no" required="" id="heat_no" value="<?php echo $heat; ?>" style=" display: none;">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Part No</label>
                                        <div class="col-md-6">                                        
                                            <input type="text" name="part_no" class="form-control part_no" required="" value="55562231" readonly="" style="color:#000;">
                                        </div>
                                    </div> 
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Date</label>
                                        <div class="col-md-6">
                                            <?php
                                            if(@$main_data->c_date)
                                            {?>
                                         <input type="text" class=" form-control datetimepicker2 c_date" name="c_date" required="" id="d1" value="<?php echo @$main_data->c_date;?>">
                                            <?php }else{
                                                $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
                                                $date1 = $date->format('Y-m-d');
                                                ?>
                                            <input type="text" class=" form-control datetimepicker2 c_date" name="c_date" required="" id="d1" value="<?php echo $date1;?>">    
                                            <?php }?>
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </div>
                        <div id="item_img" ><img src="Audio/670_img.png" style="" class=" "></div>
                    </div>
                </div>
            </div>
        </div>                    
        <div class="row">
            <div class="col-md-12">

                <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default" style="overflow-x:auto;overflow-y:auto;border: 0px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">OBSERVATIONS</h3>
                    </div>
                    <div class="panel-body">
                        <label class=" btn btn-primary addmore_670_<?php echo $count; ?>">L <i class="fa fa-plus"></i></label>
                        <br><br><br>
                        <table class="table table-bordered table_670_<?php echo $count; ?>" >

                            <tbody id="append_data_670_<?php echo $count; ?>" class="append_data_670_<?php echo $count; ?>">
                                <tr>
                                    <th>AT POINT</th>
                                    <th></th>
                                    <td colspan="2">ZONE 1 'O'</td>
                                    <td colspan="2">ZONE 1 A At 85 º</td>
                                    <td colspan="2">ZONE 1 B At 85º</td>
                                    <td colspan="2">ZONE 2 C</td>
                                    <td colspan="2">EFFECTIVE CHILL <br> DEPTH </td>
                                </tr>
                            <input type="text" name="at_point[]" value="ZONE 1 'O'" style=" display: none;">
                            <input type="text" name="at_point[]" value="ZONE 1 A At 85 º" style=" display: none;">
                            <input type="text" name="at_point[]" value="ZONE 1 B At 85º" style=" display: none;">
                            <input type="text" name="at_point[]" value="ZONE 2 C" style=" display: none;">
                            <input type="text" name="at_point[]" value="EFFECTIVE CHILL DEPTH" style=" display: none;">
                            <tr>
                                <th >CUSTOMER SPEC.</th>
                                <th >MM - HRc</th>
                                <td colspan="2">05 – 48 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">5  mm Min. All Over</td>
                            </tr>
                            <input type="text" name="cust_spec[]" value="05 .. 48 Min" style=" display: none;">
                            <input type="text" name="cust_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="cust_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="cust_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="cust_spec[]" value="5  mm Min. All Over" style=" display: none;">

                            <tr>
                                <th>WORKING SPEC.</th>
                                <th >MM - HRc</th>
                                <td colspan="2">05 – 48 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">3.5 – 40 Min.</td>
                                <td colspan="2">5  mm Min. All Over</td>
                            </tr>
                            <input type="text" name="working_spec[]" value="05 .. 48 Min" style=" display: none;">
                            <input type="text" name="working_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="working_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="working_spec[]" value="3.5 .. 40 Min" style=" display: none;">
                            <input type="text" name="working_spec[]" value="5  mm Min. All Over" style=" display: none;">
                            <?php
                            if ($main_data) {

                                $a1 = '';
                                $a3 = '';
                                foreach ($D as $row) {
                                    $a1[] = explode(",", $row->data_value);
                                    $a2[] = explode(",", $row->data_value1);
                                }
                                //print_r($a1);
                                $g = 0;
                                $L = '';
                                $L_C = '';
                                $L_C_l = '';
                                $a = '';
                                foreach ($D1 as $row) {
                                    $L_C[] = $row->L_C;
                                    $L_C_l .= $row->L_C . ",";
                                    $L[] = $row->L;
                                }
                                $L_C_l = explode(",", rtrim($L_C_l, ","));
                                for ($k = 0; $k < count($L); $k++) {
                                    $L_C1 = explode(",", $L_C[$k]);
                                    ?>

                                    <tr>
                                        <td><input type='text'  class="form-control L1_708_<?php echo json_decode($count); ?>" name="L[]" value="<?php echo $L[$k]; ?>"><input  class="btn btn-info btn-rounded add_c_<?php echo json_decode($count); ?>"  value="C-<?php echo $k; ?>"></td></tr>
                                    <?php
                                    for ($i = 0; $i < count($L_C1); $i++) {
                                        ?>
                                        <tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control a_count_670_<?php echo json_decode($count); ?>" name="L<?php echo $k; ?>[]" value="<?php echo $L_C1[$i]; ?>"></td>
                                            <?php
                                            for ($j = 0; $j < count($a1[$g]); $j++) {
                                                $action=array('48','40','40','40','5');
                                                ?>
                                                <td><input type="text" style="width:60px;" class="form-control" name="a<?php echo $g; ?>[]" value="<?php print_r($a1[$g][$j]); ?>"></td>
                                                <?php
                                                if($a2[$g][$j] < $action[$j])
                                                   {?>
                                                <td><input type="text" style="width:60px; color: red; border-color:  red;" class="form-control action<?php echo $j; ?> " name="ab<?php echo $g; ?>[]" value="<?php print_r($a2[$g][$j]); ?>"></td> 
                                                <?php }
                                                else{?>
                                                <td><input type="text" style="width:60px;" class="form-control action<?php echo $j; ?> " name="ab<?php echo $g; ?>[]" value="<?php print_r($a2[$g][$j]); ?>"></td>
                                                <?php } ?>
                                            <?php } ?></tr>    
                                            <?php
                                        $g++;
                                    }
                                }
                            }

                            if ($main_data) {
                                ?>
                                <input type="text" class=" form-control" name="hadness_id" id="hadness_id" value="<?php echo $main_data->id; ?>" style=" display: none;">    
                            <?php } else {
                                ?>
                                <input type="text" class=" form-control" name="hadness_id" id="hadness_id" value="" style=" display: none;">    
                            <?php } ?>                    
                            </tbody>
                        </table>

                        <!--------------------B----------------------------------->
                        <label class=" btn btn-danger addmore_b_670_<?php echo json_decode($count); ?>">B <i class="fa fa-plus"></i></label>
                        <br><br><br>
                        <table class="table table-bordered table_b_670_<?php echo $count; ?>" >

                            <tbody id="append_data_b_670_<?php echo $count; ?>" class="append_data_b_670_<?php echo $count; ?>">
                                <tr>
                                    <th>AT POINT</th>
                                    <th></th>
                                    <td colspan="4">BRINELL HARDNESS AT 0.5 mm FROM CAST SURFACE AFTER</td>
                                </tr>
                            <input type="text" name="at_point_b[]" value="BRINELL HARDNESS AT 0.5 mm FROM CAST SURFACE AFTER" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="BRINELL HARDNESS AT 0.5 mm FROM CAST SURFACE AFTER" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="BRINELL HARDNESS AT 0.5 mm FROM CAST SURFACE AFTER" style=" display: none;">
                            <input type="text" name="at_point_b[]" value="BRINELL HARDNESS AT 0.5 mm FROM CAST SURFACE AFTER" style=" display: none;">

                            <tr>
                                <th >CUSTOMER SPEC.</th>
                                <td>BRG 1 200 - 285</td>
                                <td>BRG 3 200 - 285</td>
                                <td>BRG 5 200 - 285</td>
                                <td>--</td>
                            </tr>

                            <input type="text" name="cust_spec_b[]" value="BRG 1 200 - 285" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG 3 200 - 285" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="BRG 5 200 - 285" style=" display: none;">
                            <input type="text" name="cust_spec_b[]" value="-" style=" display: none;">
                            <tr>
                                <th>WORKING SPEC.</th>
                                <td>BRG 1 200 - 285</td>
                                <td>BRG 3 200 - 285</td>
                                <td>BRG 5 200 - 285</td>
                                <td> CAM CORE 7 MM PDC 200 – 285 BHN</td>
                            </tr>
                            <input type="text" name="working_spec_b[]" value="BRG 1 200 - 285" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG 3 200 - 285" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="BRG 5 200 - 285" style=" display: none;">
                            <input type="text" name="working_spec_b[]" value="CAM CORE 7 MM PDC 200 – 285 BHN" style=" display: none;">

                            <?php
                            if ($D2) {
                                $a1 = '';
                                $a3 = '';
                                foreach ($D2 as $row) {
                                    $a1[] = explode(",", $row->data_value);
                                }
                                
                                $g = 0;
                                $L = '';
                                $L_C = '';
                                $L_C_l = '';
                                $a = '';
                                foreach ($D3 as $row) {
                                    $L_C[] = $row->L_C;
                                    $L_C_l .= $row->L_C . ",";
                                    $L[] = $row->L;
                                }
                                $L_C_l = explode(",", rtrim($L_C_l, ","));
                                for ($k = 0; $k < count($L); $k++) {
                                    $L_C1 = explode(",", $L_C[$k]);
                                    ?>
                                    <tr>
                                        <td><input type='text'  class="form-control L1_b_670_<?php echo json_decode($count); ?> b_count_670_<?php echo json_decode($count); ?>" name="LB[]" value="<?php echo $L[$k]; ?>"></td>
                                <?php
                                for ($i = 0; $i < count($L_C1); $i++) {
                                    ?>
                                        
                                        <?php
                                        for ($j = 0; $j < count($a1[$g]); $j++) {
                                            ?>
                                                <td><input type="text"  class="form-control" name="b<?php echo $g; ?>[]" value="<?php print_r($a1[$g][$j]); ?>"></td>         
                                            <?php } ?></tr>    
                                            <?php
                                            $g++;
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                        <!-----------------/B---------------------------------------->
                        
                        <table class="table table-bordered">
                            <tr>
                                <th><div id="item_img" ><img src="Audio/670_cam.png" style="" class=" "></div></th>
                                <th><textarea class=" form-control" name="remark" placeholder="REMARK"><?php echo @$main_data->remark; ?></textarea></th>
                            </tr>
                        </table>
                        
                        <table class="table table-bordered">
                            <tr>
                                <td>Checked By</td>
                                <td><input type="text" class="form-control approved_by " name="checked_by" value="<?php echo @$main_data->checked_by; ?>"></td>
                            </tr>
                            <tr>
                                <td >Approved By</td>
                                <td><input type="text" class="form-control approved_by " name="approved_by" value="<?php echo @$main_data->approved_by; ?>"></td>
                            </tr>
                        </table>
                        <div class="panel-footer">
                            <label class="btn btn-primary pull-right submit" id="save" name="print_option" value="" onclick="submit('670_<?php echo $count; ?>');">save</label>
                            <label class="btn btn-primary pull-left submit" id="submit" name="print_option" value="" onclick="submit('670_<?php echo $count; ?>');">Submit</label>
                            <input type="text" class="status" name="status" style=" display: none;">
                        </div> 

                    </div>
                </div>
            </div>    
        </div>
    </div>
</form>
<div id="loader_670_<?php echo $count;?>" style="  margin-left: 500px; margin-top: 800px; position: absolute; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>

<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
var all=<?php echo json_decode($count); ?>;
var all1=<?php echo json_decode($count); ?>;
all="670_" + all;
window['i_670_'+<?php echo json_decode($count); ?>] = 0; 
window['j_670_'+<?php echo json_decode($count); ?>] = -1; 
window['k_670_'+<?php echo json_decode($count); ?>] = 0; 
var k_670= 0;
var add_li = <?php echo json_decode($count); ?>;
var item_count=$("#item_count").val();
add_li= "add_l" + item_count;
var add_more;
add_more= <?php echo json_decode($count); ?>;
add_more= "addMore670_" + add_more; 
$(".addmore_670_<?php echo json_decode($count); ?>").on('click', function () {
window["addMore670_" + <?php echo json_decode($count); ?>]();

});
//
window["addMore670_" + <?php echo json_decode($count); ?>] = function() {
$.ajax({
url: "",
cache: false,
async: false,
success: function (data) {
    var l_length=0;
    l_length = document.getElementsByClassName('L1_670_'+<?php echo json_decode($count); ?>);
   var l_length_count=parseInt(l_length.length);
var data = "<tr><td><input type='text' placeholder='L-"+ l_length_count+"' class='form-control L1_670_"+<?php echo json_decode($count); ?>+"' name='L[]' ><input  class='btn btn-info btn-rounded add_c_"+<?php echo json_decode($count); ?>+" "+<?php echo json_decode($count); ?>+"_670_show_l" + window['i_670_'+<?php echo json_decode($count); ?>] + "' value='C-"+l_length_count+"' id='670_"+<?php echo json_decode($count); ?>+"_show_l" + window['i_670_'+<?php echo json_decode($count); ?>] + "'></td></tr>";
$('#append_data_670_'+<?php echo json_decode($count); ?>).append(data);
}
});
window['i_670_'+<?php echo json_decode($count); ?>]++;   
window['j_670_'+<?php echo json_decode($count); ?>]++;   
}

//

$(document).on('click', '.add_c_'+<?php echo json_decode($count); ?>, function (e) {
    var id_this=$(this).val();
    id_this=id_this.split("-");
    id_this=id_this[1];
    
    var cost=0;
    cost = document.getElementsByClassName('a_count_670_'+<?php echo json_decode($count); ?>);
   var cost_count=parseInt(cost.length);
   
$('.table_670_'+<?php echo json_decode($count); ?>+' tbody ').append('<tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control a_count_670_'+<?php echo json_decode($count); ?>+'" name="L'+id_this+'[]" placeholder="L'+window['j_670_'+<?php echo json_decode($count); ?>]+'-C-'+window['k_670_'+<?php echo json_decode($count); ?>]+'"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell" style="border-left: 2px solid #fff;"><input type="text" style="width:60px;" class="form-control action0" name="ab'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell" style="border-left: 2px solid #fff;"><input type="text" style="width:60px;" class="form-control action1" name="ab'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell" style="border-left: 2px solid #fff;"><input type="text" style="width:60px;" class="form-control action2" name="ab'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell" style="border-left: 2px solid #fff;"><input type="text" style="width:60px;" class="form-control action3" name="ab'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="a'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell" style="border-left: 2px solid #fff;"><input type="text" style="width:60px;" class="form-control action4" name="ab'+cost_count+'[]" onkeypress="return isNumber(event)"></td>\n\
<tr>');
    window['k_670_'+<?php echo json_decode($count); ?>]++; 
    });

    

    $(document).on('click', '.submit', function (e) {
if($(this).attr('id')=="submit")
{$('.status').val("1"); }
else{$('.status').val("0");}
        
var form = document.getElementById("670_<?php echo $count;?>");
for(var i=0; i < form.elements.length; i++){
if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
swal(
'Oops...',
'ALL Fields are Mandatory',
'error'
);
$('input[name='+form.elements[i].name+']').focus();
$('select[name='+form.elements[i].name+']').focus();
return;
}
}    
    $("#loader_670_"+<?php echo json_decode($count); ?>).show();
    $("#loader_670_"+<?php echo json_decode($count); ?>).fadeOut(5000);
    
    $.ajax({
    url:'<?php echo base_url(); ?>login_controller/save_data_670',
    type: 'POST',
    cache: false,
    async: false,
    data:$("#670_"+<?php echo $count; ?>).serialize(),
    success: function(data){
        
        $("#hadness_id").val(data);
    swal({
  position: 'top-end',
  type: 'success',
  title: 'Data Inserted',
  showConfirmButton: false,
  timer: 1500
});  
var he=$("#heat_no").val();
//$("#real_670_"+<?php echo json_decode($count); ?>).remove();
//$('#670_'+he+'_item').remove();
    }
    });
    });

// ------------------B--------------------------

window['k_b_670_'+<?php echo json_decode($count); ?>] = 0;
window['i_b_670_'+<?php echo json_decode($count); ?>] = 0;     
$(".addmore_b_670_<?php echo json_decode($count); ?>").on('click', function () {
window["addMore_b_670_" + <?php echo json_decode($count); ?>]();

});  

window["addMore_b_670_" + <?php echo json_decode($count); ?>] = function() {
$.ajax({
url: "",
cache: false,
async: false,
success: function (data) {
    var b_length=0;
    b_length = document.getElementsByClassName('L1_b_670_'+<?php echo json_decode($count); ?>);
   var b_length_count=parseInt(b_length.length);
var data = "<tr><td><input type='text' placeholder='L-"+ b_length_count+"' class='form-control L1_b_670_"+<?php echo json_decode($count); ?>+"' name='LB[]' ></td>\n\
<td class='recipe-table__cell'><input type='text'  class='form-control' name='b"+b_length_count+"[]'></td>\n\
<td class='recipe-table__cell'><input type='text'  class='form-control' name='b"+b_length_count+"[]'></td>\n\
<td class='recipe-table__cell'><input type='text'  class='form-control' name='b"+b_length_count+"[]'></td>\n\
<td class='recipe-table__cell'><input type='text'  class='form-control' name='b"+b_length_count+"[]'></td></tr>";
$('#append_data_b_670_'+<?php echo json_decode($count); ?>).append(data);
}
});
window['i_b_670_'+<?php echo json_decode($count); ?>]++;
window["add_b_c_" + <?php echo json_decode($count); ?>]();
}

window["add_b_c_" + <?php echo json_decode($count); ?>] = function() {

   var cost_count_b=0;
$('.table_b_670_'+<?php echo json_decode($count); ?>+' tbody ').append('<tr><td class="drag-handler"></td><td class="recipe-table__cell"><input type="text"  style="width:60px;" class="form-control b_count_670_'+<?php echo json_decode($count); ?>+'" name="LB'+id_this+'[]" placeholder="L'+window['i_b_670_'+<?php echo json_decode($count); ?>]+'-C-'+window['k_b_670_'+<?php echo json_decode($count); ?>]+'"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="b'+cost_count_b+'[]" onkeypress="return isNumber(event)"></td><td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="b'+cost_count_b+'[]" onkeypress="return isNumber(event)">\n\
    </td><td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="b'+cost_count_b+'[]" onkeypress="return isNumber(event)"></td>\n\
    <td class="recipe-table__cell"><input type="text" style="width:60px;" class="form-control" name="b'+cost_count_b+'[]" onkeypress="return isNumber(event)"></td>\n\
<tr>');
    window['k_b_670_'+<?php echo json_decode($count); ?>]++; 
    }
    
   // ---------------------/B---------------------------------------------------------------- 
    </script>
<script src='js/Sortable.js'></script>
<script>
    $(document).ready(function () {
    $(document).on('click', '.recipe-table__del-row-btn', function (e) {
    var $el = $(e.currentTarget);
    var $row = $el.closest('tr');
    $row.remove();
    return false;
    });
    Sortable.create(
    $('.append_data_670_<?php echo $count; ?>')[0],
    {
    animation: 150,
    scroll: true,
    handle: '.drag-handler',
    onEnd: function () {
    }
    }
    );
    Sortable.create(
    $('.append_data_b_670_<?php echo $count; ?>')[0],
    {
    animation: 150,
    scroll: true,
    handle: '.drag-handler',
    onEnd: function () {
    }
    }
    );
    });
    
    
        $(document).on('keyup', '.action0', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));   
            if(parseInt(th) < 48)
            {$(this).css("color", "red");  
            $(this).css("border-color", "red"); 
        }else
        {$(this).css("color", "");  
        $(this).css("border-color", "");     
        }
        });
        $(document).on('keyup', '.action1', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));
            if(parseInt(th) < 40)
            {$(this).css("color", "red");  
            $(this).css("border-color", "red"); 
        }else
        {$(this).css("color", "");  
        $(this).css("border-color", "");     
        }
        });
        $(document).on('keyup', '.action2', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));
            if(parseInt(th) < 40)
            {$(this).css("color", "red");  
            $(this).css("border-color", "red"); 
        }else
        {$(this).css("color", "");  
        $(this).css("border-color", "");     
        }
        });
        $(document).on('keyup', '.action3', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));
            if(parseInt(th) < 40)
            {$(this).css("color", "red");  
            $(this).css("border-color", "red"); 
        }else
        {$(this).css("color", "");  
        $(this).css("border-color", "");     
        }
        });
        $(document).on('keyup', '.action4', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));
            if(parseInt(th) < 5)
            {$(this).css("color", "red");  
            $(this).css("border-color", "red"); 
        }else
        {$(this).css("color", "");  
        $(this).css("border-color", "");     
        }
        });
        $(document).on('keyup', '.L1_670_<?php echo json_decode($count); ?>', function (e) {
            var th=$(this).val();
            var $this = $(this);
            $this.val($this.val().replace(/[^\d.]/g, ''));
           
        });
        
//function check_data()
//{
//  var c_date=$('.c_date').val();
//  $('.c_date').datepicker('setDate', 'today');
//}

 $(document).on('keydown', 'input', function (e) {
 if (e.which == 13  || e.keyCode == 13  ) 
 {      e.preventDefault();
        var $canfocus=$(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
}   
});
</script> 
 <script src="<?php echo base_url();?>js/datetimepicker-master/build/jquery.datetimepicker.full.js"></script>
 <script src="<?php echo base_url();?>js/datetimepicker-master/build/my.js"></script>

        
   
    

