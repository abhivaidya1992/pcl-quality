<script src="<?php echo base_url();?>js/highcharts.js"></script>
<script src="<?php echo base_url();?>js/exporting.js"></script>
<body style="font-family: Comic Sans MS, Comic Sans, cursive;" onload="show_graph();">
<div class="panel panel-default">
        
    <div class="panel-body">
        <div class="page-content-wrap">
            <div class="row">
                        <div class="col-md-5">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h3 class="panel-title"><img src="<?php echo base_url(); ?>img/ind.png" height="50" width="52"><span style=" margin-left: 200px; font-size:  25px;">DASHBOARD</span></h3>
                                </div>

                                <div class="panel-body panel-body-table">

                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
<!--                                                <tr>
                                                    <th width="100"></th>
                                                    <th class="text-center" width="50"><h2>F1</h2></th>
                                                    <th class="text-center" width="50"><h2>F2</h2></th>
                                                    <th class="text-center" width="50"><h2>F3</h2></th>
                                                    <th class="text-center" width="50"><h2>F4</h2></th>
                                                    
                                                </tr>-->
                                            </thead>
                                            <tbody>                                            
                                                <tr class="success">
                                                    <td class="text-center"><h3>TOTAL MOULD</h3></td>
                                                    <td class="text-center"><h2><div id="m1"></div></h2></td>
                                                    
                                                </tr>
                                                <tr class="info">
                                                    <td class="text-center"><h3>TOTAL MELTING AND POURING</h3></td>
                                                    <td class="text-center"><h2><div id="m2"></div></h2></td>
                                                    
                                                    
                                                </tr>
                                                <tr class="warning">
                                                    <td class="text-center"><h3>TOTAL FETTLING</h3></td>
                                                    <td class="text-center"><h2><div id="m3"></div></h2></td>
                                                    
                                                    
                                                </tr>
                                                <tr class="danger">
                                                    <td class="text-center"><h3>TOTAL TOOLING PATTERN</h3></td>
                                                    <td class="text-center"><h2><div id="m4"></div></h2></td>
                                                    
                                                    
                                                </tr>
                                                
                                                
                                            </tbody>
                                        </table>
                                    </div>                                

                                </div>
                            </div>                                                

                        </div>
                <div class="col-md-7">
                    <div id="container" style=" width: 600px;"></div>
                </div>
                    </div>
<!--            <h2>TOTAL HEAT POURED</h2>-->
<!--           <div class="row">
                        <div class="col-md-3">                        
                            <a href="#" class="tile tile-danger">
                                <div id="heat_f1"></div>
                                <p>F-1</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-info">
                                <div id="heat_f2"></div>
                                <p>F-2</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-primary">
                                <div id="heat_f3"></div>
                                <p>F-3</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-success">
                                <div id="heat_f4"></div>
                                <p>F-4</p>
                            </a>                        
                        </div>
                    </div> -->
<!--            <h2>TOTAL SHELL POURED</h2>-->
<!--           <div class="row">
                        <div class="col-md-3">                        
                            <a href="#" class="tile tile-danger">
                                <div id="shell_f1"></div>
                                <p>F-1</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-info">
                                <div id="shell_f2"></div>
                                <p>F-2</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-primary">
                                <div id="shell_f3"></div>
                                <p>F-3</p>
                            </a>                        
                        </div><div class="col-md-3">                        
                            <a href="#" class="tile tile-success">
                                <div id="shell_f4"></div>
                                <p>F-4</p>
                            </a>                        
                        </div>
                    </div>                                                             -->
        </div>
    </div>
    </div>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script>
$(document).ready(function(){
setInterval('overall_dashboard()', 1000);
setInterval('show_graph()', 20000);
}); 



function overall_dashboard()
{
$.ajax({
url:'<?php echo base_url(); ?>login_controller/overall_dashboard',

success: function(data){
//    alert(data);
//    return;
var obj = JSON.parse(data);

if(obj.m1==null)
{document.getElementById("m1").innerHTML=0;}
else{document.getElementById("m1").innerHTML=obj.m1;}

if(obj.p1==null){
document.getElementById("m2").innerHTML=0;}
else{document.getElementById("m2").innerHTML=obj.p1;}

if(obj.f1==null){
document.getElementById("m3").innerHTML=0;}
else{document.getElementById("m3").innerHTML=obj.f1;}

if(obj.pa1==null || obj.pa1==''){
document.getElementById("m4").innerHTML="0";}
else{
document.getElementById("m4").innerHTML=obj.pa1;}



}
});
}





function show_graph()
{
$.ajax({
url:'<?php echo base_url(); ?>login_controller/overall_dashboard',

success: function(data){
//    alert(data);
//    return;
var obj = JSON.parse(data);

if(obj.m1==null)
{var d1=0;}
else{d1=obj.m1;}

if(obj.p1==null){
var d2=0;}
else{d2=obj.p1;}

if(obj.f1==null){
var d3=0;}
else{d3=obj.f1;}

if(obj.pa1==null || obj.pa1==''){
var d4="0";}
else{
d4=obj.pa1;}




Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'REJECTION GRAPH'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.y:1f}</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.y:1f}',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'TOTAL MOULD',
            y: parseInt(d1),
            sliced: true,
            selected: true
        }, {
            name: 'TOTAL MELTING AND POURING',
            y: parseInt(d2)
        }, {
            name: 'TOTAL FETTLING',
            y: parseInt(d3)
        }, {
            name: 'TOTAL TOOLING PATTERN',
            y: parseInt(d4)
        }]
    }]
});
}
});
}
</script>    
</body>