<style>
table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 10px;
        text-align: center;
    }
    
    
</style>
<?php
if($data)
{?>
<div class="btn-group pull-right">
    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
    <ul class="dropdown-menu">
        <li class="divider"></li>
        <li><a href="#" onClick ="$('.customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
    </ul>
</div>
<hr>
<br><br>
<input type="button" value="Print" class="btn" id="print" onclick="printDiv('printDiv')"></input>
<br><br>
<div id="printDiv" style="width: 100%; overflow-x: scroll;">
    <table border="1" align="center" class="customers2">
        <tr>
            <td rowspan="2"><img src="<?php echo base_url(); ?>img/icn.png"></td>
            <td colspan="9" style=" text-align: center;"> <b>QUALITY SYSTEM RECORD</b></td>
            <td rowspan="2" colspan="3">PCR/IR/000/040</td>
        </tr>
        <tr>
            <td colspan="9" style=" text-align: center;"><b>CAMSHAFT SCARP ANALYSIS<br> AT FIRST STAGE & FINAL INSPECTION</b></td>
        </tr>
        <tr>
            <td align="center" colspan="2"><b>INSPECTOR: <?php echo $data->inspector;?></b></td>
            <td align="center" colspan="2"><b>ITEM: <?php echo $data->item;?></b></td>

            <td align="center" colspan="5"><b>PART NO: <?php echo $data->part_no;?> </b></td>
            <td align="center" colspan="2"><b>C/S: <?php echo $data->cs;?> </b></td>
            <td align="center"><b>SHIFT: <?php echo $data->shift;?></b> </td>
            <td align="center"><b>DATE: <?php echo $data->c_date;?> </b></td>
        </tr>
    </table>
    <br>
    <?php
        $mould_name= array(
            'LOOSE SAND','PIN HOLE','GAS HOLE','GLUE','MOULD CRACK','CORE BROKEN','TIR REJECT',
            'CHILL DEFECT','THICK FLASH','MOULD LEAK','EXTRA METAL','OTHER');  
        
        $melt_name=array(
            'HOT TEAR','COLD METAL','LC','SLAG','RIPPLE','SHORT POURED','PELTCH','OTHER'
        );
        
        $felt_name=array('CHIP COLD','CHIPHOT','EXCESS FET.','OTHER');
        
        $pattern_name=array('X - JOINT','DIGGING IN','LINEAR GAUGE REJ.','OTHER ( SIM GAP, P/M ETC.)','PPAP');
        ?>
    <!-----------Mould------>
    <table border="1" align="center" class="customers2">
            <tr>
                <td style=" width: 150px;"><b>IMP.NO.</b></td>
                <?php
                $defects = '';
                foreach ($D1 as $row) {
                    $count_row = $row->imp_no;
                    $defects[] = $row->defects;
                    $total = $row->total;
                    ?>

                <?php
                }
                $count_row = explode(",", $count_row);
                ?>
                <?php
                for ($i = 0; $i < count($count_row); $i++) {
                    ?>
                    <td><b><?php echo $count_row[$i]; ?></b></td>
                <?php } ?>
                <td><b>Total</b></td>
            </tr>

            <!--        <h3>MOULDING DEFECTS</h3>-->
            <tr>
                <td colspan="<?php echo count($defects) + 2; ?>"><h3>MOULDING DEFECTS</h3></td>
            </tr>

            <?php
            $total = explode(",", $total);
            for ($j = 0; $j < count($mould_name); $j++) {
                $d = explode(",", $defects[$j]);
                ?>
                <tr>
                    <td><b><?php echo $mould_name[$j]; ?></b></td>
                    <?php
                    for ($k = 0; $k < count($d); $k++) {
                        ?>
                        <td><?php echo $d[$k]; ?></td>
                    <?php } ?>
                    <td><?php echo $total[$j]; ?></td>
    <?php } ?>
            </tr>
<!-----------/MOULD-------------->

<!---------------------Melt----------------------->
            
                <?php
                $defects = '';
                foreach ($D2 as $row) {
                    $count_row = $row->imp_no;
                    $defects[] = $row->defects;
                    $total = $row->total;
                }
                ?>
            <tr>
                <td colspan="<?php echo count($defects) + 2; ?>"><h3>MELTING AND POURING DEFECTS</h3></td>
            </tr>

            <?php
            $total = explode(",", $total);
            for ($j = 0; $j < count($melt_name); $j++) {
                $d = explode(",", $defects[$j]);
                ?>
                <tr>
                    <td><b><?php echo $melt_name[$j]; ?></b></td>
                    <?php
                    for ($k = 0; $k < count($d); $k++) {
                        ?>
                        <td><?php echo $d[$k]; ?></td>
                    <?php } ?>
                    <td><?php echo $total[$j]; ?></td>
    <?php } ?>
            </tr>
<!-------------------/END MELT---------->

<!---------------------FELT----------------------->
            
                <?php
                $defects = '';
                foreach ($D3 as $row) {
                    $count_row = $row->imp_no;
                    $defects[] = $row->defects;
                    $total = $row->total;
                }
                   ?>
                

            <tr>
                <td colspan="<?php echo count($defects) + 2; ?>"><h3>KNOCKOUT AND FETTLING DEFECTS</h3></td>
            </tr>

            <?php
            $total = explode(",", $total);
            for ($j = 0; $j < count($felt_name); $j++) {
                $d = explode(",", $defects[$j]);
                ?>
                <tr>
                    <td><b><?php echo $felt_name[$j]; ?></b></td>
                    <?php
                    for ($k = 0; $k < count($d); $k++) {
                        ?>
                        <td><?php echo $d[$k]; ?></td>
                    <?php } ?>
                    <td><?php echo $total[$j]; ?></td>
    <?php } ?>
            </tr>
<!-------------------/END FELT---------->


<!---------------------PATTERN----------------------->
           
                <?php
                $defects = '';
                foreach ($D4 as $row) {
                    $count_row = $row->imp_no;
                    $defects[] = $row->defects;
                    $total = $row->total;
                }
                   ?>
            <tr>
                <td colspan="<?php echo count($defects) + 2; ?>"><h3>TOOLING AND PATTERN SHOP DEFECTS</h3></td>
            </tr>

            <?php
            $total = explode(",", $total);
            for ($j = 0; $j < count($pattern_name); $j++) {
                $d = explode(",", $defects[$j]);
                ?>
                <tr>
                    <td><b><?php echo $pattern_name[$j]; ?></b></td>
                    <?php
                    for ($k = 0; $k < count($d); $k++) {
                        ?>
                        <td><?php echo $d[$k]; ?></td>
                    <?php } ?>
                    <td><?php echo $total[$j]; ?></td>
    <?php } ?>
            </tr>
        </table>
<!-------------------/END PATTERN---------->

    
    
    <br>
    <table border="1" align="center" class="customers2">
        
        
        <tr>
            <td align="center" colspan="3"><b>TOTAL CHECKED: <?php echo $data->total_checked;?></b></td>

            <td align="center" colspan="5"><b>TOTAL GOOD: <?php echo $data->total_good;?></b> </td>
            <td align="center" colspan="2"><b>TOTAL REJ.: <?php echo $data->total_rej;?></b> </td>
            <td align="center"><b>FR. &CUT: <?php echo $data->fr_cut;?></b> </td>
            <td align="center"><b>MET. REJ: <?php echo $data->met_rej;?> </b></td>
        </tr>
        <tr>
            <td align="center" colspan="3"><b>TOTAL MOULDING DEFECTS: <?php echo $data->total_mould;?></b></td>

            <td align="center" colspan="5"><b>TOTAL MELTING & POURING DEFECTS.: <?php echo $data->total_pouring;?></b> </td>
            <td align="center" colspan="2"><b>TOTAL KNOCKOUT & FETTLING DEFECTS.: <?php echo $data->total_fettling;?> </b></td>
            <td align="center" colspan="2"><b>TOTAL TOOLING & PATTERN SHOP DEFECTS.: <?php echo $data->total_pattern;?> </b></td>
           
        </tr>
    </table>
</div>
<?php 
}?>
                    
<script>
function printDiv(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = "<html><head><title></title></head><body>" + printContents + "</body></html>";
window.print();
document.body.innerHTML = originalContents;
}
</script>