 <script type="text/javascript" src="<?php echo base_url();?>js/sweetalert-dev.js"></script>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url();?>css/sweetalert.css"/>
<body>  
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Report Camshaft Scrap</a></li>
</ul>
<div class="page-content-wrap">
<div class="row">
<div class="col-md-12" style=" background-color: #F0F8FF;">
<form class="form-horizontal">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><strong>Report Hardness</strong> </h3>
            <ul class="panel-controls">
                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
            </ul>
        </div>
<div class="panel-body">                                                                        
    <div class="row">
        <div class="col-md-6">


            <div class="panel-body">
                
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Date</label>
                    <div class="col-md-6">                                        
                        <input type="text" class=" form-control datetimepicker2 e_date" name="part_no" required="">
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Item</label>
                    <div class="col-md-6">                                        
                        <div id="item_list"></div>
                    </div>
                </div> 
                
                 
            </div>
        </div>
        <div class="col-md-6">


            <div class="panel-body">
                 
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Heat No</label>
                    <div class="col-md-6">                                        
                        <div id="show_part_no_data"></div>
                    </div>
                    <!--                <label class="btn btn-primary pull-right" id="show_part_no" name="print_option" value="">Next</label>-->
                </div> 
                <div class="form-group">
                    
                    
                    <label class="btn btn-primary pull-right" id="submitbutton" name="print_option" value="">Submit</label>
                </div> 
            </div>
        </div>
        
        
    </div>
    <div id="show_item_data"></div>    
    <div id="show_item_data1"></div>    
</div>
               
    </div>
</form>
</div>
</div>                    
</div>

<div id="loader" style="  margin-left: 500px; margin-top: 300px; position: fixed; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>
<div></div
<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
$(document).on('click', '#submitbutton', function (e) {


$("#loader").show();
$("#loader").fadeOut(4000);
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_hardness_report_data'); ?>",
data: {item: $('.item').val(),heat_no:$('.heat_no').val(),c_date:$('.e_date').val()},
cache: false,
async: false,
success: function (data) {
 
$('#show_item_data').html(data);

}
});
});
$(document).on('click', '.view', function (e) {


$("#loader").show();
$("#loader").fadeOut(2000);
var item_id=$(this).closest('tr').find("input.item_id").val();
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_hardness_report_data1'); ?>",
data: {id: item_id},
cache: false,
async: false,
success: function (data) {
 
$('#show_item_data1').html(data);

}
});
});


$(document).on('change', '.item', function (e) {
$("#loader").show();
$("#loader").fadeOut(1000);
var item = $('.item').val();

$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_heat_data'); ?>",
data: {item: item},
cache: false,
async: false,
success: function (data) {

$('#show_part_no_data').html(data);
$('.select2').select2();
}
});
});

$(document).on('change', '.e_date', function (e) {
$("#loader").show();
$("#loader").fadeOut(1000);
var e_date = $('.e_date').val();

$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_item_data'); ?>",
data: {e_date: e_date},
cache: false,
async: false,
success: function (data) {

$('#item_list').html(data);
$('.select2').select2();
}
});
});




</script>
</body>