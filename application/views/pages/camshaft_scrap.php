 <script type="text/javascript" src="<?php echo base_url();?>js/sweetalert-dev.js"></script>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url();?>css/sweetalert.css"/>
<style>

    body {
        
    }

    input[type="text"]:focus {
        border-color: #66afe9;
        outline: 0;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, 0.6);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, 0.6);
    }


    /* define height and width of scrollable area. Add 16px to width for scrollbar          */
    div.tableContainer {
        /*clear: both;*/
        border: 1px solid #963;
        height: 285px;
        overflow: auto;
        width: 100%;
    }

    /* Reset overflow value to hidden for all non-IE browsers. */
    html>body div.tableContainer {
        overflow: hidden;
        width: 100%;
    }

    /* define width of table. IE browsers only                 */
    div.tableContainer table {
        /*float: left;*/
        /*width: 740px*/ 
    }


    html>body div.tableContainer table {
        /* width: 756px */
    }


    thead.fixedHeader tr {
        position: relative;
    }

    thead.fixedHeader th {
/*        background: #C96;
        border-left: 1px solid #EB8;
        border-right: 1px solid #B74;
        border-top: 1px solid #EB8;
        font-weight: normal;
        padding: 4px 3px;
        text-align: left*/
    }

    html>body tbody.scrollContent {
        display: block;
        height: 262px;
        overflow: auto;
        width: 100%
    }

    html>body thead.fixedHeader {
        display: table;
        overflow: auto;
        width: 100%;
        
    }

    tbody.scrollContent td, tbody.scrollContent tr.normalRow td {
        background: #FFF;
        border-bottom: 1px solid #CCC;
       
        border-right: 1px solid #CCC;
        border-top: 1px solid #DDD;
        padding: 2px 3px 3px 4px
    }
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

</style>
<body>  
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">CAMSHAFT SCARP ANALYSIS</a></li>
</ul>
<div class="page-content-wrap">
<div class="row">
<form class="form-horizontal"  method="post" name="myForm1" id="<?php echo $item_name;?>_<?php echo $count; ?>" enctype="multipart/form-data" >
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>CAMSHAFT SCARP ANALYSIS - <?php echo $item_name;?> - <?php echo $c_date;?> - <?php echo $count; ?></strong> </h3>
            </div>
            <input type="text" name="item" value="<?php echo $item_name;?>" style=" display:  none;">
            <input type="text" name="c_date" value="<?php echo $c_date;?>" style=" display:  none;">
            <input type="text" name="cam_id"  style=" display:none;" id="cam_id">
            <div class="panel-body">                                                                        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Inspector</label>
                            <div class="col-md-5">                                            
                                <input type="text" class="form-control" name="inspector" id="inspector" required="" tabindex="1">
                            </div>
                        </div>
                        <div class="form-group">                                        
                            <label class="col-md-3 control-label">Part No</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="part_no" id="part_no" required="" tabindex="1">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">                                        
                            <label class="col-md-3 control-label">Shift</label>
                            <div class="col-md-5">
                                <select class="form-control select" required="" name="shift" required="">
                                    <option selected="" disabled="">select</option>
                                    <option value="I">I</option>
                                    <option value="II">II</option>
                                    <option value="III">III</option>
                                </select>
                            </div>            
                        </div>
                        <div class="form-group">                                        
                            <label class="col-md-3 control-label">C/S</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" name="cs" id="cs" required="" tabindex="1" >
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>    
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Defects Quality</strong> </h3>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel blank-panel">
                        <div class="panel-heading">
                            <div class="panel-title m-b-md"><h4></h4></div>
                            <div class="panel-options">
                                <ul class="nav nav-tabs">
                                    <li class="active" style=" background-color:  #1caf9a;"><a data-toggle="tab" href="#tab-1_<?php echo $count; ?>" style=" background-color:  #1caf9a;color: black;">Moulding Deffects</a></li>
                                    <li class="" style=" background-color:   #89d2c8;"><a data-toggle="tab" href="#tab-2_<?php echo $count; ?>" style=" background-color:  #89d2c8;color: black;">Melting & Pouring Deffects</a></li>
                                    <li class="" style=" background-color:  #1caf9a;"><a data-toggle="tab" href="#tab-3_<?php echo $count; ?>" style=" background-color:  #1caf9a;color: black;">Knock out & Fettling Deffects</a></li>
                                    <li class="" style=" background-color:  #89d2c8;"><a data-toggle="tab" href="#tab-4_<?php echo $count; ?>" style=" background-color:  #89d2c8;color: black;">Tooling & pattern Shop Deffects</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="tab-1_<?php echo $count; ?>" class="tab-pane animated active" >
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content" >
                                                <table class=" ">
                                                    <thead>
                                                        <tr style=" background-color:  #1caf9a;">
                                                            <th>#</th>
                                                            <th>IMP No</th>
                                                            <th>LOOSE SAND</th>
                                                            <th>PIN HOLE</th>
                                                            <th>GAS HOLE</th>
                                                            <th>GLUE</th>
                                                            <th>MOULD CRACK</th>
                                                            <th>CORE BROKEN</th>
                                                            <th>TIR REJECT</th>
                                                            <th>CHILL DEFECT</th>
                                                            <th>THICK FLASH</th>
                                                            <th>MOULD LEAK</th>
                                                            <th>EXTRA METAL</th>
                                                            <th>OTHER</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="append_ipcs_<?php echo $count; ?>">


                                                    </tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td><h3>Total</h3></td>
                                                        <td><input type="text" class=" form-control" id="d1_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d2_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d3_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d4_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d5_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d6_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d7_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d8_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d9_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d10_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d11_<?php echo $count; ?>" readonly=""  name="total_defects_by_mould[]"style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d12_<?php echo $count; ?>" readonly="" name="total_defects_by_mould[]" style="color: black;"></td>

                                                    </tr>
                                                </table>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" id="addMore_ipcs_<?php echo $count; ?>" class='btn btn-success btn-lg addMore_ipcs_<?php echo $count; ?>' style="border-radius: 50px; background-color:  #1caf9a;"><i class="fa fa-plus-square"></i></button><br>
                                    </div>
                                </div>
                                <div id="tab-2_<?php echo $count; ?>" class="tab-pane animated" >
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content" >
                                                <table class=" "> 
                                                    <thead>
                                                        <tr style=" background-color:  #89d2c8;">
                                                            <th>#</th>
                                                            <th>IMP No</th>
                                                            <th>HOT TEAR</th>
                                                            <th>COLD METAL</th>
                                                            <th>LC</th>
                                                            <th>SLAG</th>
                                                            <th>RIPPLE</th>
                                                            <th>SHORT POURED</th>
                                                            <th>PELTCH</th>
                                                            <th>OTHER</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="append_ipcs2_<?php echo $count; ?>">
                                                    </tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td><h3>Total</h3></td>
                                                        <td><input type="text" class=" form-control " id="d13_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d14_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d15_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d16_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d17_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d18_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d19_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d20_<?php echo $count; ?>" readonly="" name="total_defects_by_melt[]" style="color: black;"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" id="addMore_ipcs_<?php echo $count; ?>" class='btn btn-success btn-lg addMore_ipcs_<?php echo $count; ?>' style="border-radius: 50px; background-color:  #1caf9a;"><i class="fa fa-plus-square"></i></button><br>
                                    </div>

                                </div>
                                <div id="tab-3_<?php echo $count; ?>" class="tab-pane animated" >
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content" >
                                                <table class=" ">
                                                    <thead>
                                                        <tr style=" background-color:  #1caf9a;">
                                                            <th>#</th>
                                                            <th>IMP No</th>
                                                            <th>CHIP COLD</th>
                                                            <th>CHIPHOT</th>
                                                            <th>EXCESS FET.</th>
                                                            <th>OTHER</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="append_ipcs3_<?php echo $count; ?>">


                                                    </tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td><h3>Total</h3></td>

                                                        <td><input type="text" class=" form-control " id="d21_<?php echo $count; ?>" readonly="" name="total_defects_by_felt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d22_<?php echo $count; ?>" readonly="" name="total_defects_by_felt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d23_<?php echo $count; ?>" readonly="" name="total_defects_by_felt[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d24_<?php echo $count; ?>" readonly="" name="total_defects_by_felt[]" style="color: black;"></td>
                                                    </tr>
                                                </table>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" id="addMore_ipcs_<?php echo $count; ?>" class='btn btn-success btn-lg addMore_ipcs_<?php echo $count; ?>' style="border-radius: 50px; background-color:  #1caf9a;"><i class="fa fa-plus-square"></i></button><br>
                                    </div>

                                </div>
                                <div id="tab-4_<?php echo $count; ?>" class="tab-pane animated" >
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content" >
                                                <table class=" ">
                                                    <thead>
                                                        <tr style=" background-color:  #89d2c8;">
                                                            <th>#</th>
                                                            <th>IMP No</th>
                                                            <th>X - JOINT</th>
                                                            <th>DIGGING IN</th>
                                                            <th>LINEAR GAUGE REJ.</th>
                                                            <th>OTHER ( SIM GAP, P/M ETC.)</th>
                                                            <th>PPAP</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="append_ipcs4_<?php echo $count; ?>">
                                                    </tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td><h3>Total</h3></td>
                                                        <td><input type="text" class=" form-control " id="d25_<?php echo $count; ?>" readonly="" name="total_defects_by_pattern[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d26_<?php echo $count; ?>" readonly="" name="total_defects_by_pattern[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d27_<?php echo $count; ?>" readonly="" name="total_defects_by_pattern[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d28_<?php echo $count; ?>" readonly="" name="total_defects_by_pattern[]" style="color: black;"></td>
                                                        <td><input type="text" class=" form-control " id="d29_<?php echo $count; ?>" readonly="" name="total_defects_by_pattern[]" style="color: black;"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" id="addMore_ipcs_<?php echo $count; ?>" class='btn btn-success btn-lg addMore_ipcs_<?php echo $count; ?>' style="border-radius: 50px; background-color:  #1caf9a;"><i class="fa fa-plus-square"></i></button><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Total</strong> </h3>
                </div>
                <div class="panel-body">                                                                        
                    <div class="row">
                        <div class="col-md-6">
                            
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL GOOD</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_good" id="total_good_<?php echo $count; ?>" required="" tabindex="1">
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">FR. &CUT</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="fr_cut" id="fr_cut_<?php echo $count; ?>" required="" tabindex="1" value="0">
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">MET. REJ</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="met_rej" id="met_rej_<?php echo $count; ?>" required="" tabindex="1" value="0">
                                </div>
                            </div>

                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL REJ</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_rej" id="total_rej_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">TOTAL CHECKED</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_checked" id="total_checked_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;">
                                </div>
                            </div>
                            
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL MOULDING DEFECTS</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control total_mould_<?php echo $count; ?>" name="total_mould" id="total_mould_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;" value="0">
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL MELTING & POURING DEFECTS.</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_pouring" id="total_pouring_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;" value="0">
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL KNOCKOUT & FETTLING DEFECTS</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_fettling" id="total_fettling_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;" value="0">
                                </div>
                            </div>
                            <div class="form-group">                                        
                                <label class="col-md-3 control-label">TOTAL TOOLING & PATTERN SHOP DEFECTS</label>
                                <div class="col-md-5">                                            
                                    <input type="text" class="form-control" name="total_pattern" id="total_pattern_<?php echo $count; ?>" required="" tabindex="1" readonly="" style=" color:  black;" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>    
            <div class="panel panel-default">
                <div class="panel-footer">
                            <label class="btn btn-primary pull-right submit_<?php echo json_decode($count); ?>" id="save" name="print_option" value="" >save</label>
                            <label class="btn btn-primary pull-left submit_<?php echo json_decode($count); ?>" id="submit" name="print_option" value="" >Submit</label>
                            <input type="text" class="status" name="status" style=" display: none;">
                        </div>       
            </div>
            </div>
        </div> 
    </form>
    </div>
<div id="loader" style="  margin-left: 500px; margin-top: -400px; position: absolute; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>

<script type='text/javascript' language='javascript'>
 
//var j = 0;
window['j_'+<?php echo json_decode($count); ?>] = -1; 
$(".addMore_ipcs_"+<?php echo $count; ?>).on('click', function () {
//addMore_ipcs();
//addMore_ipcs3();
//addMore_ipcs4();
//addMore_ipcs2();
window["addMore_ipcs_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs3_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs4_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs2_" + <?php echo json_decode($count); ?>]();
});
window['k_'+<?php echo json_decode($count); ?>] = -1;
$(".addMore_ipcs2_"+<?php echo $count; ?>).on('click', function () {
window["addMore_ipcs_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs3_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs4_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs2_" + <?php echo json_decode($count); ?>]();
});
window['l_'+<?php echo json_decode($count); ?>] = -1;
$(".addMore_ipcs3_"+<?php echo $count; ?>).on('click', function () {
window["addMore_ipcs_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs3_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs4_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs2_" + <?php echo json_decode($count); ?>]();
});
window['m_'+<?php echo json_decode($count); ?>] = -1;
$(".addMore_ipcs4_"+<?php echo $count; ?>).on('click', function () {
window["addMore_ipcs_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs3_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs4_" + <?php echo json_decode($count); ?>]();
window["addMore_ipcs2_" + <?php echo json_decode($count); ?>]();
});


window["addMore_ipcs_" + <?php echo json_decode($count); ?>] = function() {
//function addMore_ipcs() {

$.ajax({
url: "",
success: function (data) {
var data = "<tr><td>"+ window['j_'+<?php echo json_decode($count); ?>] +"</td>";
    data += "<td><input type='tel' class='form-control imp_no_"+<?php echo json_decode($count); ?>+" f_imp_"+<?php echo json_decode($count); ?>+"' name='imp_no_mould[]' id='"+<?php echo json_decode($count); ?>+"imp_1" + window['j_'+<?php echo json_decode($count); ?>] + "' required='' style='width:60px;'> </td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d1_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a0[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de1_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d2_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a1[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de2_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d3_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a2[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de3_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d4_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a3[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de4_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d5_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a4[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de5_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d6_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a5[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de6_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d7_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a6[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de7_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d8_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a7[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de8_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d9_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a8[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de9_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d10_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a9[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de10_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d11_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a10[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de11_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" j1_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d12_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_mould_a11[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de12_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";

data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='LOOSE SAND'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='PIN HOLE'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='GAS HOLE'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='GLUE'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='MOULD CRACK'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='CORE BROKEN'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='TIR REJECT'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='CHILL DEFECT'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='THICK FLASH'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='MOULD LEAK'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='EXTRA METAL'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_mould[]' value='OTHER'></td>";


$('#append_ipcs_'+ <?php echo json_decode($count); ?>).append(data);
$('.timepicker').timepicker();
//$('#imp_no' + window['j_'+<?php echo json_decode($count); ?>]).focus();

}
});
window['j_'+<?php echo json_decode($count); ?>]++;
}


window["addMore_ipcs2_" + <?php echo json_decode($count); ?>] = function() {
//function addMore_ipcs2() {

$.ajax({
url: "",
success: function (data) {
var data = "<tr><td>"+ window['k_'+<?php echo json_decode($count); ?>] +"</td>";
    data += "<td><input type='tel' class='form-control imp_2_"+<?php echo json_decode($count); ?>+"' name='imp_no_melt[]' id='"+<?php echo json_decode($count); ?>+"imp_2"+ window['k_'+<?php echo json_decode($count); ?>] + "' required='' style='width:60px;'> </td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d13_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a0[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de13_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d14_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a1[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de14_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d15_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a2[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de15_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d16_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a3[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de16_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d17_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a4[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de17_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d18_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a5[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de18_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d19_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a6[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de19_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" k2_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d20_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_melt_a7[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de20_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    
    
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='HOT TEAR'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='COLD METAL'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='LC'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='SLAG'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='RIPPLE'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='SHORT POURED'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='PELTCH'></td>";
data += "<td style='display:none;'><input type='text' name='defect_name_melt[]' value='OTHER'></td>";


$('#append_ipcs2_'+ <?php echo json_decode($count); ?>).append(data);
$('.timepicker').timepicker();
//$('#imp_no' + window['k_'+<?php echo json_decode($count); ?>]).focus();

}
});
window['k_'+<?php echo json_decode($count); ?>]++;
}
window["addMore_ipcs3_" + <?php echo json_decode($count); ?>] = function() {
//function addMore_ipcs3() {
$.ajax({
url: "",
success: function (data) {
var data = "<tr><td>"+ window['l_'+<?php echo json_decode($count); ?>] +"</td>";
    data += "<td><input type='tel' class='form-control imp_3_"+<?php echo json_decode($count); ?>+"' name='imp_no_felt[]' id='"+<?php echo json_decode($count); ?>+"imp_3" + window['l_'+<?php echo json_decode($count); ?>] + "' required='' style='width:60px;'> </td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" l3_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d21_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_felt_a0[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de21_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" l3_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d22_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_felt_a1[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de22_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" l3_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d23_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_felt_a2[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de23_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" l3_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d24_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_felt_a3[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de24_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_felt[]' value='CHIP COLD'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_felt[]' value='CHIPHOT'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_felt[]' value='EXCESS FET.'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_felt[]' value='OTHER'></td>";

 
$('#append_ipcs3_'+ <?php echo json_decode($count); ?>).append(data);
$('.timepicker').timepicker();
//$('#imp_no' + window['l_'+<?php echo json_decode($count); ?>]).focus();

}
});
window['l_'+<?php echo json_decode($count); ?>]++;
}

window["addMore_ipcs4_" + <?php echo json_decode($count); ?>] = function() {
//function addMore_ipcs4() {
$.ajax({
url: "",
success: function (data) {
var data = "<tr><td>"+ window['m_'+<?php echo json_decode($count); ?>] +"</td>";
    data += "<td><input type='tel' class='form-control imp_4_"+<?php echo json_decode($count); ?>+"' name='imp_no_pattern[]' id='"+<?php echo json_decode($count); ?>+"imp_4" + window['m_'+<?php echo json_decode($count); ?>] + "' required='' style='width:60px;'> </td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" m4_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d25_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_pattern_a0[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de25_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" m4_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d26_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_pattern_a1[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de26_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" m4_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d27_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_pattern_a2[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de27_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" m4_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d28_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_pattern_a3[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de28_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    data += "<td class='s1_"+<?php echo json_decode($count); ?>+" m4_"+<?php echo json_decode($count); ?>+"' style='display:none;'><input class='btn btn-success incre_"+<?php echo json_decode($count); ?>+" d29_"+<?php echo json_decode($count); ?>+"' readonly=''  style='width:50px;' name='defects_pattern_a4[]' value='0'><label class='btn btn-danger decre_"+<?php echo json_decode($count); ?>+" de29_"+<?php echo json_decode($count); ?>+"' style='width:50px;'>-</label></td>";
    
    data += "<td style='display:none;'><input type='text' name='defect_name_pattern[]' value='X - JOINT'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_pattern[]' value='DIGGING IN'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_pattern[]' value='LINEAR GAUGE REJ.'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_pattern[]' value='OTHER ( SIM GAP, P/M ETC.)'></td>";
    data += "<td style='display:none;'><input type='text' name='defect_name_pattern[]' value='PPAP'></td>";

$('#append_ipcs4_'+ <?php echo json_decode($count); ?>).append(data);
$('.timepicker').timepicker();
//$('#imp_no' + window['m_'+<?php echo json_decode($count); ?>]).focus();

}
});
window['m_'+<?php echo json_decode($count); ?>]++;
}


//

$(document).on('click', '.incre_'+<?php echo json_decode($count); ?>, function (e) {
var val=$(this).val();
if(val=='' || val==null){
$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val('1');}
else{val=parseInt(val) + 1;
$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val(val);}
});

$(document).on('click', '.decre_'+<?php echo json_decode($count); ?>, function (e) {
var val=$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val();  
if(val=='' || val==null)
{$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val('0');}
else{if(val <= 0){
$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val('0');}
else{val=parseInt(val) - 1;
$(this).closest('td').find("input.incre_"+<?php echo json_decode($count); ?>).val(val); }
}
});
// D1
$(document).on('click','.d1_'+<?php echo json_decode($count); ?>,function(e){

var sum=0;var cost=0;
var id_name='d1_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
//alert(sum);
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_mould();
window["total_mould_" + <?php echo json_decode($count); ?>]();

});
$(document).on('click','.d2_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d2_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d3_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d3_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d4_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d4_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d5_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d5_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d6_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d6_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d7_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d7_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d8_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d8_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d9_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d9_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d10_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d10_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d11_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d11_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d12_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d12_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d13_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d13_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_pouring();
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d14_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d14_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d15_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d15_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d16_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d16_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d17_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d17_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d18_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d18_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d19_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d19_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d20_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d20_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d21_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d21_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_fettling();
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d22_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d22_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d23_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d23_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d24_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d24_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d25_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d25_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_pattern();
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d26_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d26_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d27_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d27_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d28_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d28_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.d29_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d29_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
// End D1

// De1
$(document).on('click','.de1_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d1_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_mould();
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de2_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d2_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de3_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d3_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de4_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d4_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de5_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d5_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de6_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d6_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de7_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d7_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de8_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d8_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de9_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d9_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de10_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d10_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de11_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d11_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de12_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d12_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_mould_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de13_'+<?php echo json_decode($count); ?>,function(e){
    
var sum=0;var cost=0;
var id_name='d13_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_pouring();
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de14_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d14_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de15_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d15_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de16_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d16_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de17_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d17_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de18_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d18_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de19_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d19_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de20_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d20_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pouring_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de21_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d21_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_fettling();
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de22_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d22_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de23_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d23_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de24_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d24_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_fettling_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de25_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d25_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
//total_pattern();
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de26_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d26_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de27_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d27_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de28_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d28_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});
$(document).on('click','.de29_'+<?php echo json_decode($count); ?>,function(e){
var sum=0;var cost=0;
var id_name='d29_'+<?php echo json_decode($count); ?>;
cost = document.getElementsByClassName(id_name);
for(var i=0;i<cost.length;i++)
{sum = parseInt(sum) + parseInt(cost[i].value);}
if(sum!="NaN"){document.getElementById(id_name).value = sum;}
else{document.getElementById(id_name).value = "0";}
window["total_pattern_" + <?php echo json_decode($count); ?>]();
});


window["total_mould_" + <?php echo json_decode($count); ?>] = function() {
//function total_mould()
//{
    
var sum1=0;
var cost1=0;
var sum2=0;
var cost2=0;
var sum3=0;
var cost3=0;
var sum4=0;
var cost4=0;
var sum5=0;
var cost5=0;
var sum6=0;
var cost6=0;
var sum7=0;
var cost7=0;
var sum8=0;
var cost8=0;
var sum9=0;
var cost9=0;
var sum10=0;
var cost10=0;
var sum11=0;
var cost11=0;
var sum12=0;
var cost12=0;
var final_total=0;
var cost1 = document.getElementsByClassName('d1_'+<?php echo json_decode($count); ?>);
var cost2 = document.getElementsByClassName('d2_'+<?php echo json_decode($count); ?>);
var cost3 = document.getElementsByClassName('d3_'+<?php echo json_decode($count); ?>);
var cost4 = document.getElementsByClassName('d4_'+<?php echo json_decode($count); ?>);
var cost5 = document.getElementsByClassName('d5_'+<?php echo json_decode($count); ?>);
var cost6 = document.getElementsByClassName('d6_'+<?php echo json_decode($count); ?>);
var cost7 = document.getElementsByClassName('d7_'+<?php echo json_decode($count); ?>);
var cost8 = document.getElementsByClassName('d8_'+<?php echo json_decode($count); ?>);
var cost9 = document.getElementsByClassName('d9_'+<?php echo json_decode($count); ?>);
var cost10 = document.getElementsByClassName('d10_'+<?php echo json_decode($count); ?>);
var cost11 = document.getElementsByClassName('d11_'+<?php echo json_decode($count); ?>);
var cost12 = document.getElementsByClassName('d12_'+<?php echo json_decode($count); ?>);

for(var i=0;i<cost1.length;i++)
{sum1 = parseInt(sum1) + parseInt(cost1[i].value);}

for(var i=0;i<cost2.length;i++)
{sum2 = parseInt(sum2) + parseInt(cost2[i].value);}

for(var i=0;i<cost3.length;i++)
{sum3 = parseInt(sum3) + parseInt(cost3[i].value);}

for(var i=0;i<cost4.length;i++)
{sum4 = parseInt(sum4) + parseInt(cost4[i].value);}

for(var i=0;i<cost5.length;i++)
{sum5 = parseInt(sum5) + parseInt(cost5[i].value);}

for(var i=0;i<cost6.length;i++)
{sum6 = parseInt(sum6) + parseInt(cost6[i].value);}

for(var i=0;i<cost7.length;i++)
{sum7 = parseInt(sum7) + parseInt(cost7[i].value);}

for(var i=0;i<cost8.length;i++)
{sum8 = parseInt(sum8) + parseInt(cost8[i].value);}

for(var i=0;i<cost9.length;i++)
{sum9 = parseInt(sum9) + parseInt(cost9[i].value);}

for(var i=0;i<cost10.length;i++)
{sum10 = parseInt(sum10) + parseInt(cost10[i].value);}

for(var i=0;i<cost11.length;i++)
{sum11 = parseInt(sum11) + parseInt(cost11[i].value);}

for(var i=0;i<cost12.length;i++)
{sum12 = parseInt(sum12) + parseInt(cost12[i].value);}

final_total=parseInt(sum1) + parseInt(sum2) + parseInt(sum3)
+ parseInt(sum4) + parseInt(sum5) + parseInt(sum6) + parseInt(sum7)
+ parseInt(sum8) + parseInt(sum9) + parseInt(sum10) + parseInt(sum11)
+ parseInt(sum12);

if(final_total!="NaN"){document.getElementById('total_mould_'+<?php echo json_decode($count); ?>).value = final_total;}
else{document.getElementById('total_mould_'+<?php echo json_decode($count); ?>).value = "0";}
window["other_total" + <?php echo json_decode($count); ?>]();
}

window["total_pouring_" + <?php echo json_decode($count); ?>] = function() {
//function total_pouring (){
var sum1=0;
var cost1=0;
var sum2=0;
var cost2=0;
var sum3=0;
var cost3=0;
var sum4=0;
var cost4=0;
var sum5=0;
var cost5=0;
var sum6=0;
var cost6=0;
var sum7=0;
var cost7=0;
var sum8=0;
var cost8=0;
;
var final_total=0;
var cost1 = document.getElementsByClassName('d13_'+<?php echo json_decode($count); ?>);
var cost2 = document.getElementsByClassName('d14_'+<?php echo json_decode($count); ?>);
var cost3 = document.getElementsByClassName('d15_'+<?php echo json_decode($count); ?>);
var cost4 = document.getElementsByClassName('d16_'+<?php echo json_decode($count); ?>);
var cost5 = document.getElementsByClassName('d17_'+<?php echo json_decode($count); ?>);
var cost6 = document.getElementsByClassName('d18_'+<?php echo json_decode($count); ?>);
var cost7 = document.getElementsByClassName('d19_'+<?php echo json_decode($count); ?>);
var cost8 = document.getElementsByClassName('d20_'+<?php echo json_decode($count); ?>);


for(var i=0;i<cost1.length;i++)
{sum1 = parseInt(sum1) + parseInt(cost1[i].value);}

for(var i=0;i<cost2.length;i++)
{sum2 = parseInt(sum2) + parseInt(cost2[i].value);}

for(var i=0;i<cost3.length;i++)
{sum3 = parseInt(sum3) + parseInt(cost3[i].value);}

for(var i=0;i<cost4.length;i++)
{sum4 = parseInt(sum4) + parseInt(cost4[i].value);}

for(var i=0;i<cost5.length;i++)
{sum5 = parseInt(sum5) + parseInt(cost5[i].value);}

for(var i=0;i<cost6.length;i++)
{sum6 = parseInt(sum6) + parseInt(cost6[i].value);}

for(var i=0;i<cost7.length;i++)
{sum7 = parseInt(sum7) + parseInt(cost7[i].value);}

for(var i=0;i<cost8.length;i++)
{sum8 = parseInt(sum8) + parseInt(cost8[i].value);}



final_total=parseInt(sum1) + parseInt(sum2) + parseInt(sum3)
+ parseInt(sum4) + parseInt(sum5) + parseInt(sum6) + parseInt(sum7)
+ parseInt(sum8);

if(final_total!="NaN"){document.getElementById('total_pouring_'+<?php echo json_decode($count); ?>).value = final_total;}
else{document.getElementById('total_pouring_'+<?php echo json_decode($count); ?>).value = "0";}
window["other_total" + <?php echo json_decode($count); ?>]();
}

window["total_fettling_" + <?php echo json_decode($count); ?>] = function() {
//function total_fettling(){
var sum1=0;
var cost1=0;
var sum2=0;
var cost2=0;
var sum3=0;
var cost3=0;
var sum4=0;
var cost4=0;


var final_total=0;
var cost1 = document.getElementsByClassName('d21_'+<?php echo json_decode($count); ?>);
var cost2 = document.getElementsByClassName('d22_'+<?php echo json_decode($count); ?>);
var cost3 = document.getElementsByClassName('d23_'+<?php echo json_decode($count); ?>);
var cost4 = document.getElementsByClassName('d24_'+<?php echo json_decode($count); ?>);



for(var i=0;i<cost1.length;i++)
{sum1 = parseInt(sum1) + parseInt(cost1[i].value);}

for(var i=0;i<cost2.length;i++)
{sum2 = parseInt(sum2) + parseInt(cost2[i].value);}

for(var i=0;i<cost3.length;i++)
{sum3 = parseInt(sum3) + parseInt(cost3[i].value);}

for(var i=0;i<cost4.length;i++)
{sum4 = parseInt(sum4) + parseInt(cost4[i].value);}





final_total=parseInt(sum1) + parseInt(sum2) + parseInt(sum3)
+ parseInt(sum4);

if(final_total!="NaN"){document.getElementById('total_fettling_'+<?php echo json_decode($count); ?>).value = final_total;}
else{document.getElementById('total_fettling_'+<?php echo json_decode($count); ?>).value = "0";}
window["other_total" + <?php echo json_decode($count); ?>]();
}
window["total_pattern_" + <?php echo json_decode($count); ?>] = function() {
//function total_pattern(){
var sum1=0;
var cost1=0;
var sum2=0;
var cost2=0;
var sum3=0;
var cost3=0;
var sum4=0;
var cost4=0;
var sum5=0;
var cost5=0;


var final_total=0;
var cost1 = document.getElementsByClassName('d25_'+<?php echo json_decode($count); ?>);
var cost2 = document.getElementsByClassName('d26_'+<?php echo json_decode($count); ?>);
var cost3 = document.getElementsByClassName('d27_'+<?php echo json_decode($count); ?>);
var cost4 = document.getElementsByClassName('d28_'+<?php echo json_decode($count); ?>);
var cost5 = document.getElementsByClassName('d29_'+<?php echo json_decode($count); ?>);



for(var i=0;i<cost1.length;i++)
{sum1 = parseInt(sum1) + parseInt(cost1[i].value);}

for(var i=0;i<cost2.length;i++)
{sum2 = parseInt(sum2) + parseInt(cost2[i].value);}

for(var i=0;i<cost3.length;i++)
{sum3 = parseInt(sum3) + parseInt(cost3[i].value);}

for(var i=0;i<cost4.length;i++)
{sum4 = parseInt(sum4) + parseInt(cost4[i].value);}

for(var i=0;i<cost5.length;i++)
{sum5 = parseInt(sum5) + parseInt(cost5[i].value);}

final_total=parseInt(sum1) + parseInt(sum2) + parseInt(sum3)
+ parseInt(sum4) + parseInt(sum5);

if(final_total!="NaN"){document.getElementById('total_pattern_'+<?php echo json_decode($count); ?>).value = final_total;}
else{document.getElementById('total_pattern_'+<?php echo json_decode($count); ?>).value = "0";}
window["other_total" + <?php echo json_decode($count); ?>]();
}










$(document).on('keyup','.imp_no_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val()){
var final=0;
var cost1=0;
var cost1 = document.getElementsByClassName('f_imp_'+<?php echo json_decode($count); ?>); 
for(var i=0;i<cost1.length;i++)
{
    if(cost1[i].value=='' || cost1[i].value==null)
    {final = ''; }
    else
    {final = parseInt(cost1[i].value);}
    
$('#'+<?php echo json_decode($count); ?>+'imp_2' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_3' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_4' + i).val(final); 
$('.k2_'+<?php echo json_decode($count); ?>).show(); 
$('.l3_'+<?php echo json_decode($count); ?>).show(); 
$('.m4_'+<?php echo json_decode($count); ?>).show(); 

}
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  
}else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
});

$(document).on('keyup','.imp_2_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val()){
var final=0;
var cost1=0;
var cost1 = document.getElementsByClassName('imp_2_'+<?php echo json_decode($count); ?>); 
for(var i=0;i<cost1.length;i++)
{
    if(cost1[i].value=='' || cost1[i].value==null)
    {final = ''; }
    else
    {final = parseInt(cost1[i].value);}
$('#'+<?php echo json_decode($count); ?>+'imp_1' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_3' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_4' + i).val(final); 
$('.j1_'+<?php echo json_decode($count); ?>).show(); 
$('.l3_'+<?php echo json_decode($count); ?>).show(); 
$('.m4_'+<?php echo json_decode($count); ?>).show(); 
}
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  
}else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
});


$(document).on('keyup','.imp_3_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val()){
var final=0;
var cost1=0;
var cost1 = document.getElementsByClassName('imp_3_'+<?php echo json_decode($count); ?>); 
for(var i=0;i<cost1.length;i++)
{
    if(cost1[i].value=='' || cost1[i].value==null)
    {final = ''; }
    else
    {final = parseInt(cost1[i].value);}
$('#'+<?php echo json_decode($count); ?>+'imp_1' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_2' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_4' + i).val(final); 
$('.j1_'+<?php echo json_decode($count); ?>).show(); 
$('.k2_'+<?php echo json_decode($count); ?>).show(); 
$('.m4_'+<?php echo json_decode($count); ?>).show(); 
}
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  
}else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
});


$(document).on('keyup','.imp_4_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val()){
var final=0;
var cost1=0;
var cost1 = document.getElementsByClassName('imp_4_'+<?php echo json_decode($count); ?>); 
for(var i=0;i<cost1.length;i++)
{
    if(cost1[i].value=='' || cost1[i].value==null)
    {final = ''; }
    else
    {final = parseInt(cost1[i].value);}
$('#'+<?php echo json_decode($count); ?>+'imp_1' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_3' + i).val(final); 
$('#'+<?php echo json_decode($count); ?>+'imp_2' + i).val(final); 
$('.j1_'+<?php echo json_decode($count); ?>).show(); 
$('.k2_'+<?php echo json_decode($count); ?>).show(); 
$('.l3_'+<?php echo json_decode($count); ?>).show(); 
}
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  
}else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
});










$(document).on('keyup','.imp_2_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val())
{
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide(); }
});
$(document).on('keyup','.imp_3_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val())
{
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide(); }
});
$(document).on('keyup','.imp_4_'+<?php echo json_decode($count); ?>,function(e){
if($(this).val())
{
$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show();  }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide(); }
});








$(document).on('click','.submit_'+<?php echo json_decode($count); ?>,function(e){
if($(this).attr('id')=="submit")
        {$('.status').val("1"); }
        else{$('.status').val("0");}      
var form_id=<?php echo $item_name;?>+'_'+<?php echo $count; ?>;
var form = document.getElementById(form_id);
for(var i=0; i < form.elements.length; i++){
if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
swal(
'Oops...',
'Please Select ' + form.elements[i].name,
'error'
);
return false;
}
}
$("#loader").show();
$("#loader").fadeOut(5000);


$.ajax({
url:'<?php echo base_url(); ?>login_controller/save_camshaft_scrap',
type: 'POST',
data: $("#"+form_id).serialize(),
success: function(data){
$("#cam_id").val(data);
    swal({
  position: 'top-end',
  type: 'success',
  title: 'Data Inserted',
  showConfirmButton: false,
  timer: 1500
}); 
}
});
});


$(".delete").on('click', function () {
$('.case:checkbox:checked').parents("tr").remove();
$('.check_all').prop("checked", false);
check();
});



$(document).on('keyup', '.f_imp_'+<?php echo json_decode($count); ?>, function (e) {
var current_value = $(this).val();
$(this).attr('value',current_value);
console.log(current_value);
if ($('.f_imp_'+'<?php echo json_decode($count); ?>[value="' + current_value + '"]').not($(this)).length > 0  ) {
$(this).focus();
if($(this).val())
{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show(); }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
swal(
'Oops...',
'Please Enter Unique IMP No ',
'error'
);
}
});

$(document).on('keyup', '.imp_2_'+<?php echo json_decode($count); ?>, function (e) {
var current_value = $(this).val();
$(this).attr('value',current_value);
console.log(current_value);
if ($('.imp_2_'+'<?php echo json_decode($count); ?>[value="' + current_value + '"]').not($(this)).length > 0  ) {
$(this).focus();
if($(this).val())
{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show(); }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
swal(
'Oops...',
'Please Enter Unique IMP No ',
'error'
);
}
});
$(document).on('keyup', '.imp_3_'+<?php echo json_decode($count); ?>, function (e) {
var current_value = $(this).val();
$(this).attr('value',current_value);
console.log(current_value);
if ($('.imp_3_'+'<?php echo json_decode($count); ?>[value="' + current_value + '"]').not($(this)).length > 0  ) {
$(this).focus();
if($(this).val())
{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show(); }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
swal(
'Oops...',
'Please Enter Unique IMP No ',
'error'
);
}
});
$(document).on('keyup', '.imp_4_'+<?php echo json_decode($count); ?>, function (e) {
var current_value = $(this).val();
$(this).attr('value',current_value);
console.log(current_value);
if ($('.imp_4_'+'<?php echo json_decode($count); ?>[value="' + current_value + '"]').not($(this)).length > 0  ) {
$(this).focus();
if($(this).val())
{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).show(); }
else{$(this).closest('tr').find("td.s1_"+<?php echo json_decode($count); ?>).hide();}
swal(
'Oops...',
'Please Enter Unique IMP No ',
'error'
);
}
});
window["other_total" + <?php echo json_decode($count); ?>] = function()
//function other_total()
{
var total_good= $("#total_good_"+<?php echo json_decode($count); ?>).val();
var fr_cut=$("#fr_cut_"+<?php echo json_decode($count); ?>).val();
var met_rej=$("#met_rej_"+<?php echo json_decode($count); ?>).val();
var total_rej=$("#total_rej_"+<?php echo json_decode($count); ?>).val();
var total_checked=$("#total_checked_"+<?php echo json_decode($count); ?>).val();
var total_mould=$("#total_mould_"+<?php echo json_decode($count); ?>).val();
var total_pouring=$("#total_pouring_"+<?php echo json_decode($count); ?>).val();
var total_pattern=$("#total_pattern_"+<?php echo json_decode($count); ?>).val();
var total_fettling=$("#total_fettling_"+<?php echo json_decode($count); ?>).val();
total_rej=parseInt(total_mould) + parseInt(total_pouring) + parseInt(total_fettling) 
+ parseInt(total_pattern) + parseInt(fr_cut) + parseInt(met_rej);
$("#total_rej_"+<?php echo json_decode($count); ?>).val(total_rej);
if(total_good=="" || total_good==null)
{total_good=0;}

total_good=parseInt(total_rej) + parseInt(total_good);
$("#total_checked_"+<?php echo json_decode($count); ?>).val(total_good);
}

$(document).on('keyup', '#total_good_'+<?php echo json_decode($count); ?>, function (e) {
    if($(this).val()=='' || $(this).val()==null)
    {
     $(this).val(0);   
    }
var total_good= $("#total_good_"+<?php echo json_decode($count); ?>).val();
var total_rej=$("#total_rej_"+<?php echo json_decode($count); ?>).val();
if(total_good=="" || total_good==null)
{total_good=0;}
total_good=parseInt(total_rej) + parseInt(total_good);
$("#total_checked_"+<?php echo json_decode($count); ?>).val(total_good);   
});

$(document).on('keyup', '#fr_cut_'+<?php echo json_decode($count); ?>, function (e) {
    if($(this).val()=='' || $(this).val()==null)
    {
     $(this).val(0);   
    }
var total_good= $("#total_good_"+<?php echo json_decode($count); ?>).val();
var fr_cut=$("#fr_cut_"+<?php echo json_decode($count); ?>).val();
var met_rej=$("#met_rej_"+<?php echo json_decode($count); ?>).val();
var total_rej=$("#total_rej_"+<?php echo json_decode($count); ?>).val();
var total_checked=$("#total_checked_"+<?php echo json_decode($count); ?>).val();
var total_mould=$("#total_mould_"+<?php echo json_decode($count); ?>).val();
var total_pouring=$("#total_pouring_"+<?php echo json_decode($count); ?>).val();
var total_pattern=$("#total_pattern_"+<?php echo json_decode($count); ?>).val();
var total_fettling=$("#total_fettling_"+<?php echo json_decode($count); ?>).val();
if(fr_cut=='' || fr_cut==null || met_rej=='' || met_rej==null)
{fr_cut=0;
met_rej=0;
}
total_rej=parseInt(total_mould) + parseInt(total_pouring) + parseInt(total_fettling) 
+ parseInt(total_pattern) + parseInt(fr_cut) + parseInt(met_rej);

var fr_cut1=parseInt(total_rej);
if(total_good=='' || total_good==null)
{total_good=0;
}
total_checked=parseInt(total_rej) + parseInt(total_good);
$("#total_rej_"+<?php echo json_decode($count); ?>).val(fr_cut1);
$("#total_checked_"+<?php echo json_decode($count); ?>).val(total_checked);    
});


$(document).on('keyup', '#met_rej_'+<?php echo json_decode($count); ?>, function (e) {
    
    if($(this).val()=='' || $(this).val()==null)
    {
     $(this).val(0);   
    }
var total_good= $("#total_good_"+<?php echo json_decode($count); ?>).val();
var fr_cut=$("#fr_cut_"+<?php echo json_decode($count); ?>).val();
var met_rej=$("#met_rej_"+<?php echo json_decode($count); ?>).val();
var total_rej=$("#total_rej_"+<?php echo json_decode($count); ?>).val();
var total_checked=$("#total_checked_"+<?php echo json_decode($count); ?>).val();
var total_mould=$("#total_mould_"+<?php echo json_decode($count); ?>).val();
var total_pouring=$("#total_pouring_"+<?php echo json_decode($count); ?>).val();
var total_pattern=$("#total_pattern_"+<?php echo json_decode($count); ?>).val();
var total_fettling=$("#total_fettling_"+<?php echo json_decode($count); ?>).val();
if(fr_cut=='' || fr_cut==null || met_rej=='' || met_rej==null)
{met_rej=0;
fr_cut=0;
}

total_rej=parseInt(total_mould) + parseInt(total_pouring) + parseInt(total_fettling) 
+ parseInt(total_pattern) + parseInt(fr_cut) + parseInt(met_rej);

var met_rej1=parseInt(total_rej);
if(total_good=='' || total_good==null)
{total_good=0;
}

total_checked=parseInt(total_rej) + parseInt(total_good);
$("#total_rej_"+<?php echo json_decode($count); ?>).val(met_rej1);
$("#total_checked_"+<?php echo json_decode($count); ?>).val(total_checked);    
});
</script>

</body>



