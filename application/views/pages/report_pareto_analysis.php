<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/pareto.js"></script>
<script src="<?php echo base_url();?>js/exporting.js"></script
 <script type="text/javascript" src="<?php echo base_url();?>js/sweetalert-dev.js"></script>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url();?>css/sweetalert.css"/>
<body>  
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Report Pareto Analysis</a></li>
</ul>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal"  method="post" name="myForm1" id="myForm1" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Report Pareto Analysis</strong> </h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Select Month</label>
                                        <div class="col-md-6">
                                            <select class="select2 form-control month">
                                                <option value="">Select</option>
                                                <option value="May">May-2018</option>
                                                
                                            </select>
                                            </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group" id="show">
                                        <label class="col-md-3 control-label">Select Item No</label>
                                        <div class="col-md-6">
                                            <select class="select2 form-control item">
                                                <option value="">Select</option>
                                                <option value="670">670</option>
                                                
                                            </select>
                                            </div>
                                        <label class="btn btn-primary pull-right" id="submitbutton" name="print_option" value="">Submit</label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div id="show_item_data">
                            
                        </div>    
                    </div>
                </div>
            </form>
        </div>
    </div>                    
</div>

<div id="loader" style="  margin-left: 500px; margin-top: 300px; position: fixed; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>
<div></div
<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
$(document).on('click', '#submitbutton', function (e) {

var item = $('.month').val();
if(item=='' || item==null)
{
swal(
'Oops...',
'Please Select Month',
'error'
);
return false;   
}
//$("#loader").show();
//$("#loader").fadeOut(4000);
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_pareto_analysis_data'); ?>",
data: {},
cache: false,
async: false,
success: function (data) {
 //alert(data);
$('#show_item_data').html(data);

}
});
});




</script>
</body>