<style>
table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 10px;
        text-align: center;
    }
    
    
</style>
<?php
if($data)
{?>
<div class="btn-group pull-right">
    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
    <ul class="dropdown-menu">
        <li class="divider"></li>
        <li><a href="#" onClick ="$('.customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
    </ul>
</div>
<hr>
<br><br>
<input type="button" value="Print" class="btn" id="print" onclick="printDiv('printDiv')"></input>
<br><br>
<div id="printDiv" style="width: 100%; overflow-x: scroll;">
    <table border="1" align="center" class="customers2">
        <tr>
            <td rowspan="2"><img src="<?php echo base_url(); ?>img/icn.png"></td>
            <td colspan="9" style=" text-align: center;"> <b>QUALITY SYSTEM RECORD</b></td>
            <td rowspan="2" colspan="3">PCR/IR/000/040</td>
        </tr>
        <tr>
            <td colspan="9" style=" text-align: center;"><b>DAILY REJECTION ANALYSIS REPORT</b></td>
        </tr>
        <tr>
            <td align="center" colspan="2"><b>PART NO. : <?php echo $data->item;?></b></td>
            <td align="center" colspan="2"><b>TARGET %</b></td>

            <td align="center" colspan="5"><b>FOR THE DAY</b></td>
            <td align="center" colspan="2"><b>CUMULATIVE</b></td>
        </tr>
        <tr>
            <td align="center" colspan="2"><b>QUANTITY  CHECKED</b></td>
            <td align="center" colspan="2"><b></b></td>
            <td align="center" colspan="5"><b><?php echo $data->total_checked;?></b></td>
            <td align="center" colspan="2"><b><?php echo $total_all->total_checked;?></b></td>
            
        </tr>
        <tr>
            <td align="center" colspan="2"><b>GOOD QUANTITY</b></td>
            <td align="center" colspan="2"><b></b></td>
            <td align="center" colspan="5"><b><?php echo $data->total_good;?></b></td>
            <td align="center" colspan="2"><b><?php echo $total_all->total_good;?></b></td>
            
        </tr>
        <tr>
         <td align="center" colspan="2"><b>QUANTITY REJECTED</b></td>
            <td align="center" colspan="2"><b></b></td>
            <td align="center" colspan="5"><b><?php echo $data->total_rej;?></b></td>
            <td align="center" colspan="2"><b><?php echo $total_all->total_rej;?></b></td>
        </tr>
        <tr>
           <td align="center" colspan="2"><b>%  REJECTION</b></td>
           <td align="center" colspan="2"><b></b></td>
            <td align="center" colspan="5"><b></b></td>
            <td align="center" colspan="2"><b></b></td>
        </tr>
        
    </table>
    <table border="1" align="center" class="customers2">
        <tr>
            <th><b>REJECTION  DETAILS</b></th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th colspan="2">DAY</th>
            <th colspan="2">CUMULATIVE</th>
        </tr>
        <tr>
            <th>Sr.No</th>
            <th>DEFECT</th>
            <th>TARGET %</th>
            <th>NOS.</th>
            <th>%</th>
            <th>NOS.</th>
            <th>%</th>
        </tr>
        <!------- MOULD ----------------->
        <?php
        $target_mould=array('1.60','0.50','0.10','0.20','0.05','0.05','0.30','0.05','0.10','0.30','0.20','0.10');
        $i=0;
        foreach ($day_mould as $row)
        {
        $total_defect_day[]=$row->defects_sum;
        $total_defect_day_per[]=number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');
        $total_cumm[]=$cumm_mould[$i]->total_sum_mould;
        $total_cumm_per[]=number_format((float)(($cumm_mould[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');
            ?>
        <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row->defect_name;?></td>
        <td><?php echo $target_mould[$i];?></td>
        <td><?php echo $row->defects_sum;?></td>
        <td><?php echo number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');?></td>
        <td><?php echo $cumm_mould[$i]->total_sum_mould;?></td>
        <td><?php echo number_format((float)(($cumm_mould[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');?></td>
        </tr>
        <?php $i++;} ?>
        <tr>
        <th></th>
        <th>TOTAL MOULD</th>
        <th>3.55</th>
        <th><?php echo array_sum($total_defect_day);?></th>
        <th><?php echo array_sum($total_defect_day_per);?></th>
        <th><?php echo array_sum($total_cumm);?></th>
        <th><?php echo array_sum($total_cumm_per);?></th>
        </tr>
        <!-----------------/ MOULD------------------->
        
        <!------- MELT ----------------->
        <?php
        $target_melt=array('','0.50','0.30','0.40','0.30','0.10','0.30','0.10');
        $i=0;
        foreach ($day_melt as $row)
        {
        $total_defect_day_melt[]=$row->defects_sum;
        $total_defect_day_per_melt[]=number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');
        $total_cumm_melt[]=$cumm_melt[$i]->total_sum_mould;
        $total_cumm_per_melt[]=number_format((float)(($cumm_melt[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');
            ?>
        <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row->defect_name;?></td>
        <td><?php echo $target_melt[$i];?></td>
        <td><?php echo $row->defects_sum;?></td>
        <td><?php echo number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');?></td>
        <td><?php echo $cumm_melt[$i]->total_sum_mould;?></td>
        <td><?php echo number_format((float)(($cumm_melt[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');?></td>
        </tr>
        <?php $i++;} ?>
        <tr>
        <th></th>
        <th>TOTAL MELT</th>
        <th>2.50</th>
        <th><?php echo array_sum($total_defect_day_melt);?></th>
        <th><?php echo array_sum($total_defect_day_per_melt);?></th>
        <th><?php echo array_sum($total_cumm_melt);?></th>
        <th><?php echo array_sum($total_cumm_per_melt);?></th>
        </tr>
        <!-----------------/ MELT------------------->
        
        
        <!------- FELT ----------------->
        <?php
        $target_felt=array('0.60','0.10','0.50','');
        $i=0;
        foreach ($day_felt as $row)
        {
        $total_defect_day_felt[]=$row->defects_sum;
        $total_defect_day_per_felt[]=number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');
        $total_cumm_felt[]=$cumm_felt[$i]->total_sum_mould;
        $total_cumm_per_felt[]=number_format((float)(($cumm_felt[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');
            ?>
        <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row->defect_name;?></td>
        <td><?php echo $target_felt[$i];?></td>
        <td><?php echo $row->defects_sum;?></td>
        <td><?php echo number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');?></td>
        <td><?php echo $cumm_felt[$i]->total_sum_mould;?></td>
        <td><?php echo number_format((float)(($cumm_felt[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');?></td>
        </tr>
        <?php $i++;} ?>
        <tr>
        <th></th>
        <th>TOTAL FETTLING</th>
        <th>1.20</th>
        <th><?php echo array_sum($total_defect_day_felt);?></th>
        <th><?php echo array_sum($total_defect_day_per_felt);?></th>
        <th><?php echo array_sum($total_cumm_felt);?></th>
        <th><?php echo array_sum($total_cumm_per_felt);?></th>
        </tr>
        <!-----------------/ FELT------------------->
        
        <!------- PATTERN ----------------->
        <?php
        $target_pattern=array('','0.05','','','');
        $i=0;
        foreach ($day_pattern as $row)
        {
        $total_defect_day_pattern[]=$row->defects_sum;
        $total_defect_day_per_pattern[]=number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');
        $total_cumm_pattern[]=$cumm_pattern[$i]->total_sum_mould;
        $total_cumm_per_pattern[]=number_format((float)(($cumm_pattern[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');
            ?>
        <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row->defect_name;?></td>
        <td><?php echo $target_pattern[$i];?></td>
        <td><?php echo $row->defects_sum;?></td>
        <td><?php echo number_format((float)(($row->defects_sum / $data->total_checked) * 100), 2, '.', '');?></td>
        <td><?php echo $cumm_pattern[$i]->total_sum_mould;?></td>
        <td><?php echo number_format((float)(($cumm_pattern[$i]->total_sum_mould / $total_all->total_checked) * 100), 2, '.', '');?></td>
        </tr>
        <?php $i++;} ?>
        <tr>
        <th></th>
        <th>TOTAL TOOLING & P/S</th>
        <th>0.05</th>
        <th><?php echo array_sum($total_defect_day_pattern);?></th>
        <th><?php echo array_sum($total_defect_day_per_pattern);?></th>
        <th><?php echo array_sum($total_cumm_pattern);?></th>
        <th><?php echo array_sum($total_cumm_per_pattern);?></th>
        </tr>
        
        <tr>
            <th></th>
            <th>TOTAL REJECTION</th>
            <th>7.30</th>
            <th><?php echo $total_rej=array_sum($total_defect_day) + array_sum($total_defect_day_melt) + 
                    array_sum($total_defect_day_felt)
                    + array_sum($total_defect_day_pattern);?></th>
            <th><?php echo number_format((float)(($total_rej/$data->total_checked)*100), 2, '.', '');?></th>
            <th><?php echo $total_rej1=array_sum($total_cumm) + array_sum($total_cumm_melt) + 
                    array_sum($total_cumm_felt)
                    + array_sum($total_cumm_pattern);?></th>
            <th><?php echo $total_rej2=array_sum($total_cumm_per) + array_sum($total_cumm_per_melt) + 
                    array_sum($total_cumm_per_felt)
                    + array_sum($total_cumm_per_pattern);?></th>
        </tr>
        <tr>
            <th></th>
            <th>FR-CUT( DES. TEST)</th>
            <th>1.20</th>
            <th><?php echo $data->fr_cut;?></th>
            <th><?php echo number_format((float)(($data->fr_cut/$data->total_checked)*100), 2, '.', '');?></th>
            <th><?php echo $total_all->total_fr_cut;?></th>
            <th><?php echo number_format((float)(($total_all->total_fr_cut/$total_all->total_checked)*100), 2, '.', '');?></th>
        </tr>
        <tr>
            <th></th>
            <th>TOTAL SCRAP</th>
            <th>8.50</th>
            <th><?php echo $total_rej + $data->fr_cut;?></th>
            <th><?php echo number_format((float)(($total_rej/$data->total_checked)*100), 2, '.', '') + number_format((float)(($data->fr_cut/$data->total_checked)*100), 2, '.', '');?></th>
            <th><?php echo $total_rej1 + $total_all->total_fr_cut;?></th>
            <th><?php echo $total_rej2 + number_format((float)(($total_all->total_fr_cut/$total_all->total_checked)*100), 2, '.', '');?></th>
        </tr>
        <!-----------------/ PATTERN------------------->
        
    </table>
    
</div>
<?php 
}?>
                    
<script>
function printDiv(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = "<html><head><title></title></head><body>" + printContents + "</body></html>";
window.print();
document.body.innerHTML = originalContents;
}
</script>