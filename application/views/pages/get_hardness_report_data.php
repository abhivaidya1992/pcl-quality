<style>
table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 10px;
        text-align: center;
    }
    
    
</style>
<?php
if($data)
{?>
<div class="btn-group pull-right">
    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown" id="btnExport"><i class="fa fa-bars"></i> Export Data</button>
<!--    <ul class="dropdown-menu">
        <li class="divider"></li>
        <li><a href="#" onClick ="$('.customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
    </ul>-->
</div>
<hr>
<br><br>
<input type="button" value="Print" class="btn" id="print" onclick="printDiv('printDiv')"></input>
<br><br>
<div id="printDiv" style="width: 100%; overflow-x: scroll;">
    <table border="1" align="center" class="customers2">
        <tr>
            <td rowspan="2"><img src="<?php echo base_url(); ?>img/icn.png"></td>
            <td colspan="9" style=" text-align: center;"> <b>QUALITY SYSTEM RECORD</b></td>
            <td rowspan="2" colspan="3">Q.A./REF. NO./<br>PCR/IR/<br>162/101/<br>PAGE 1 OF 1</td>
        </tr>
        <tr>
            <td colspan="9" style=" text-align: center;"><b>HARDNESS & CHILLDEPTH REPORT</b></td>
        </tr>
        <tr>
            <td align="center" colspan="3"><b>ITEM: </b></td>

            <td align="center" colspan="5"  ><b>PART NO.:  </b></td>
            <td align="center" colspan="2"><b>DATE:  </b></td>
            <td align="center"><b>HEAT NO: </b> </td>
            
        </tr>
        <tr>
            <td align="center" colspan="3"><b><?php echo $data->item;?> </b></td>

            <td align="center" colspan="5"><b><?php echo $data->part_no;?></b></td>
            <td align="center" colspan="2"><b><?php echo $data->c_date;?></b></td>
            <td align="center"><b><?php echo $data->heat_no;?></b> </td>
            
        </tr>
        
    </table>
<!--    <table>
        <tr>
        </tr>
    </table>-->
    <br>
    <table border="1" align="center" class="customers2">
            <tr>
                <th>AT POINT</th>
                <th>CUSTOMER SPEC.</th>
                <th>WORKING SPEC.</th>
                <?php
                foreach ($D2 as $row) {
                    $col_l_c = explode(",", $row->L_C);
                    ?>
                    <th colspan="<?php echo count($col_l_c); ?>">L - <?php echo $row->L; ?></th>
                <?php } ?>
</tr>
            <tr>
                <th></th>
                <th>MM - HRc</th>
                <th>MM - HRc</th>
                <?php
                $col_l_c1 = '';
                foreach ($D2 as $row) {
                    $col_l_c1 .= $row->L_C . ",";
                }
                ?>
                <?php
                $col_l_c1 = explode(",", rtrim($col_l_c1, ","));
                for ($i = 0; $i < count($col_l_c1); $i++) {
                    ?>
                    <th><?php echo $col_l_c1[$i]; ?></th>
                <?php } ?>
            </tr>
            <?php
            $main = '';
            foreach ($D1 as $row) {
                $main[] = $row->data_value;
                $main_second[] = $row->data_value1;
            }
            
            for ($j = 0; $j < count($main); $j++) {
                $main1[]= explode(",", $main[$j]);
                $main_second1[]= explode(",", $main_second[$j]);
            }
            $g=0;
            //print_r($main1);
            for ($k = 0; $k < count($D3); $k++) {
                ?>
                <tr>
<td><?php echo $D3[$k]->at_point; ?></td>    
                    <td><?php echo $D3[$k]->cust_spec; ?></td>    
                    <td><?php echo $D3[$k]->working_spec; ?></td>
                    <?php
                    for($l=0;$l<count($main1);$l++)
                    {
                    ?>
                    <td><?php print_r($main1[$l][$g]." - ".$main_second1[$l][$g]);?></td>
                    <?php } ?>
                </tr>
                <?php $g++;} ?>
                
                <!------------------------B---------------->
                <tr>
                <th>AT POINT</th>
                <th>CUSTOMER SPEC.</th>
                <th>WORKING SPEC.</th>
                <?php
                foreach ($D5 as $row) {
                    $col_l_c = explode(",", $row->L_C);
                    ?>
                    <th colspan="<?php echo count($col_l_c); ?>"><?php echo $row->L; ?></th>
                <?php } ?>
</tr>
<?php if($data->item!="670")
{?>

                <tr>
                <th></th>
                <th>MM - HRc</th>
                <th>MM - HRc</th>

                <?php
                $col_l_c1 = '';
                foreach ($D5 as $row) {
                    $col_l_c1 .= $row->L_C . ",";
                }
                ?>
<?php
                $col_l_c1 = explode(",", rtrim($col_l_c1, ","));
                
                for ($i = 0; $i < count($col_l_c1); $i++) {
                    ?>
                    <th><?php echo $col_l_c1[$i]; ?></th>
                <?php } ?>
            </tr>
<?php } ?>
                <?php
            $main_b = '';
            foreach ($D4 as $row) {
                $main_b[] = $row->data_value;
            }
            
            for ($j_b = 0; $j_b < count($main_b); $j_b++) {
                $main1_b[]= explode(",", $main_b[$j_b]);
            }
            $g_b=0;
            for ($k_b = 0; $k_b < count($D6); $k_b++) {
                ?>
                <tr>

                    <td><?php echo $D6[$k_b]->at_point; ?></td>    
                    <td><?php echo $D6[$k_b]->cust_spec; ?></td>    
                    <td><?php echo $D6[$k_b]->working_spec; ?></td>
                    <?php
                    
                    for($l_b=0;$l_b<count($main1_b);$l_b++)
                    {
                    ?>
                    <td><?php print_r($main1_b[$l_b][$g_b]);?></td>
                    <?php } ?>
                </tr>
                <?php $g_b++;} ?>
                
                <!----------------/B------------------------------------>
        </table>
    
    <table border="1" align="center">
        <tr>
        <th border='1'>CHECKED BY : <?php echo $data->checked_by;?></th>
       <th>APPROVED BY : <?php echo $data->approved_by;?></th>
       <th>Remark : <?php echo $data->remark;?></th>
       
    </tr>
    </table>
    <br><br><br>
    <table>
        <tr>
            <th><div id="item_img" ><img src="<?php echo base_url();?>/Audio/<?php echo $data->item;?>_cam.png" width="200"></div></th>
    <th><div id="item_img" ><img src="<?php echo base_url();?>/Audio/<?php echo $data->item;?>_img.png" style="" width="400"></div></th>

        </tr>

    </table>
</div>
<?php 
}?>
                    
<script>
function printDiv(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = "<html><head><title></title></head><body>" + printContents + "</body></html>";
window.print();
document.body.innerHTML = originalContents;
}




$("#btnExport").click(function(e) {
  let file = new Blob([$('#printDiv').html()], {type:"application/vnd.ms-excel"});
let url = URL.createObjectURL(file);
let a = $("<a />", {
  href: url,
  download: "filename.xls"}).appendTo("body").get(0).click();
  e.preventDefault();
});



</script>

