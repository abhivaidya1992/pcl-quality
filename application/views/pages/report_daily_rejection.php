 <script type="text/javascript" src="<?php echo base_url();?>js/sweetalert-dev.js"></script>
<link rel="stylesheet" type="text/css" id="theme" href="<?php echo base_url();?>css/sweetalert.css"/>
<body>  
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Report Camshaft Scrap</a></li>
</ul>
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal"  method="post" name="myForm1" id="myForm1" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Report Daily Rejection</strong> </h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">                                                                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Start Date</label>
                                        <div class="col-md-6">                                        
                                            <input type="text" class=" form-control datetimepicker2 s_date" name="part_no" required="">
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel-body">
                                    <div class="form-group" id="show" style=" display: none;">
                                        <label class="col-md-3 control-label">Select Item No</label>
                                        <div id="show_part_no_data"></div>
                                        <label class="btn btn-primary pull-right" id="submitbutton" name="print_option" value="">Submit</label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div id="show_item_data"></div>    
                    </div>
                </div>
            </form>
        </div>
    </div>                    
</div>

<div id="loader" style="  margin-left: 500px; margin-top: 300px; position: fixed; width: 100px; display:  none;"><img src="loading.gif" style=" width: 150px;"></div>
<div></div
<script type="text/javascript" src="<?php echo base_url();?>js/plugins/jquery/jquery.min.js"></script>
<script>
$(document).on('click', '#submitbutton', function (e) {

var item = $('.item').val();
if(item=='' || item==null)
{
swal(
'Oops...',
'Please Select item',
'error'
);
return false;   
}
//$("#loader").show();
//$("#loader").fadeOut(4000);
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_daily_rejection_data'); ?>",
data: {item: item,s_date:$('.s_date').val()},
cache: false,
async: false,
success: function (data) {
 //alert(data);
$('#show_item_data').html(data);

}
});
});
function show_all_part_no () {
//$("#loader").show();
//$("#loader").fadeOut(4000);
var s_date = $('.s_date').val();
var e_date = $('.e_date').val();
var shift = $('.shift').val();
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_part_no'); ?>",
data: {s_date: s_date,e_date:e_date,shift:shift},
cache: false,
async: false,
success: function (data) {


$('#show').show();
$('#show_part_no_data').html(data);
$('.select2').select2();
}
});
}

$(document).on('change', '.s_date', function (e) {
$("#loader").show();
$("#loader").fadeOut(2000);
var s_date = $('.s_date').val();

$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_part_no'); ?>",
data: {s_date: s_date},
cache: false,
async: false,
success: function (data) {


$('#show').show();
$('#show_part_no_data').html(data);
$('.select2').select2();
}
});
});
$(document).on('change', '.e_date', function (e) {
$("#loader").show();
$("#loader").fadeOut(4000);
var s_date = $('.s_date').val();
var e_date = $('.e_date').val();
var shift = $('.shift').val();
$.ajax({
type: "post",
url: "<?php echo base_url('login_controller/get_part_no'); ?>",
data: {s_date: s_date,e_date:e_date,shift:shift},
cache: false,
async: false,
success: function (data) {


$('#show').show();
$('#show_part_no_data').html(data);
$('.select2').select2();
}
});
});



</script>
</body>