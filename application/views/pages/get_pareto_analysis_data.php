
<style>
table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 10px;
        text-align: center;
    }
    #container {
    max-width: 900px;
    height: 600px;
    margin: 1em auto;
}
    
</style>
<?php
if($pareto_data)
{
    $defects_name='';
    $rej='';
    $Cumm_Contribution='';
    foreach ($pareto_data as $row)
    {
     $defects_name.="'".$row->DEFECT."',";  
     $rej.=$row->Rejn.",";  
     $Cumm_Contribution.=$row->Cumm_Contribution.",";  
     //print_r($Cumm_Contribution);
    }
    
    ?>
<div class="btn-group pull-right">
    <button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Data</button>
    <ul class="dropdown-menu">
        <li class="divider"></li>
        <li><a href="#" onClick ="$('.customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a></li>
    </ul>
</div>
<hr>
<br><br>
<input type="button" value="Print" class="btn" id="print" onclick="printDiv('printDiv')"></input>
<br><br>
<div id="printDiv" style="width: 100%; overflow-x: scroll;">
    <table border="1" align="center" class="customers2">
        <tr>
            <td rowspan="2"><img src="<?php echo base_url(); ?>img/icn.png"></td>
            <td colspan="9" style=" text-align: center;"> <b>PRECISION CAMSHAFT LTD. UNIT - 1</b></td>
            <td rowspan="5" colspan="3">PCR/IR/000/040</td>
        </tr>
        <tr>
            <td colspan="9" style=" text-align: center;"><b>Part No. - 0670  c/s</b></td>
        </tr>
        <tr>
            <td>CHECKED QTY :- 49189 </td>
           
            <td colspan="9"> REJECTION ANALYSIS REPORT</td>
        </tr>
        <tr>
            <td>GOOD QTY :- 46426</td>
             <td colspan="9">MONTH :- May 2018</td>
        </tr>
        <tr>
            <td>REJECTED QTY :- 2763</td>
             <td colspan="9">DATE :- 04.06.2018</td>
        </tr>
        <tr>
            <th>DEFECT</th>
            <th>Rejected qty.</th>
            <th>Rejn.%</th>
            <th>contribution %</th>
            <th colspan="9">Cumm. Contribution</th>
        </tr>
        <?php
            foreach ($pareto_data1 as $row)
            {?>
        <tr>
        <td><?php echo $row->DEFECT;?></td>
        <td><?php echo $row->Rejected_qty;?></td>
        <td><?php echo $row->Rejn;?></td>
        <td><?php echo $row->contribution;?></td>
        <td colspan="9"><?php echo $row->Cumm_Contribution;?></td>
        </tr>
            <?php } ?>
    </table>
    <div id="container"></div>
</div>
<?php 
}?>
                    
<script>
function printDiv(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = "<html><head><title></title></head><body>" + printContents + "</body></html>";
window.print();
document.body.innerHTML = originalContents;
}
Highcharts.chart('container', {
  chart: {
    renderTo: 'container',
    type: 'column'
  },
  title: {
    text: 'Pareto Analysis Report'
  },
  xAxis: {
    categories: [<?php echo rtrim($defects_name,',');?>],
    
  },
  yAxis: [{
    title: {
      text: ''
    }
    
  }, {
    title: {
      text: ''
    },
    minPadding: 0,
    maxPadding: 0,
    max: 100,
    min: 0,
    opposite: true,
    labels: {
      format: "{value}%"
    }
  }],
      
  series: [{
    type: 'pareto',
    name: 'Cumm.Contribution',
    yAxis: 1,
    zIndex: 10,
    data: [<?php echo rtrim($Cumm_Contribution,',');?>],
    color: '#080884'
  }, {
    name: 'Rejn.%',
    type: 'column',
    zIndex: 2,
    data: [<?php echo rtrim($rej,',');?>],
    color: '#9999ff'
  }],
  plotOptions: {
       series: {
       borderWidth: 0,
       dataLabels: {
       enabled: true,
       format: '{point.y:1f}',
       
       }
       }
       },
       exporting: {
            chartOptions: {
              navigator: {
                enabled: false
              },
              scrollbar: {
                enabled: false
              }
            }
        }
   
});
</script>