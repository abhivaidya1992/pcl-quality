<body style="font-family: Comic Sans MS, Comic Sans, cursive;">
<div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="index.html"><?php echo $username = $this->session->userdata('user_name'); ?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="assets/images/users/avatar.png" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="assets/images/users/avatar.png" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?php echo $username = $this->session->userdata('user_name'); ?></div>
<!--                                <div class="profile-data-title">Web Developer/Designer</div>-->
                            </div>
<!--                            <div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>-->
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Quality Form</li>
<!--                    <li class="">
                        <a href="<?php echo base_url();?>hardness_chilldept"><span class="fa fa-envelope"></span> <span class="xn-text">Hardness & Chilldepth</span></a>                        
                    </li>-->
                    <li class="">
                        <a href="<?php echo base_url();?>add_hardness_item"><span class="fa fa-envelope"></span> <span class="xn-text">Hardness & Chilldepth</span></a>                        
                    </li>
                    <li class="">
                        <a href="<?php echo base_url();?>add_camshaft_scrap"><span class="fa fa-envelope"></span> <span class="xn-text">Camshaft Scrap</span></a>                        
                    </li>
<!--                    <li class="">
                        <a href="<?php echo base_url();?>camshaft_scrap"><span class="fa fa-group"></span> <span class="xn-text btn btn-x">Camshaft Scrap</span></a>                        
                    </li> -->
                    <li class="">
                        <a href="<?php echo base_url();?>camshaft_scrap_dash"><span class="fa fa-dashboard"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li> 
                     
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-sort-amount-desc"></span> Quality Report</a>
                        <ul>                                    
                            <li><a href="<?php echo base_url(); ?>report_camshaft_scrap">Camshaft Scrap Report</a></li>
                            <li><a href="<?php echo base_url(); ?>report_daily_rejection">Daily Rejection Report</a></li>
                            <li><a href="<?php echo base_url(); ?>report_pareto_analysis">Report Pareto Analysis</a></li>
                            <li><a href="<?php echo base_url(); ?>report_hardeness">Hardness Report</a></li>

                        </ul>
                    </li>                   
                    
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>