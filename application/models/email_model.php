<?php

class Email_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    
    
    
    
    public function save_holiday_planning_form()
    {
        unset($_POST["print_option"]);
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');
        $data = $_POST;
        $created_by = $this->session->userdata('user_name');
        $created_on = $date1;
        $target_dir = "Audio/";
        $target_dir1 = "Audio/";

        $target_file = $target_dir . basename($_FILES["audio_attach"]["name"]);
        $target_file1 = $target_dir1 . basename($_FILES["file_attach"]["name"]);

        move_uploaded_file($_FILES["audio_attach"]["tmp_name"], $target_file);
        move_uploaded_file($_FILES["file_attach"]["tmp_name"], $target_file1);
        
        $array = array(
            'evalution_date' => $_POST["evalution_date"],
            'auditor' => $_POST["auditor"],
            'emp_id' => $_POST["emp_id"],
            'advisor' => $_POST["advisor"],
            'team_leader' => $_POST["team_leader"],
            'location' => $_POST["location"],
            'call_date' => $_POST["call_date"],
            'call_timing' => $_POST["call_timing"],
            'call_track_id' => $_POST["call_track_id"],
            'call_duration' => $_POST["call_duration"],
            'member_id' => $_POST["member_id"],
            'interaction_id' => $_POST["interaction_id"],
            'accurate_interaction_type' => $_POST["accurate_interaction_type"],
            'accurate_interaction_subtype' => $_POST["accurate_interaction_subtype"],
            'member_satisfaction_status' => $_POST["member_satisfaction_status"],
            'feedback_status' => $_POST["feedback_status"],
            'feedback_date' => $_POST["feedback_date"],
            'created_on' => $created_on,
            'created_by' => $created_by,
            'audio_attach'=>$_FILES['audio_attach']['name'],
            'file_attach'=>$_FILES['file_attach']['name'],
        );
        $this->db->insert('tbl_holiday_paln_audit_sheet', $array);
        $last_id = $this->db->insert_id();
        
        $array2= array(
            'holiday_plan_audit_sheet_id'=>$last_id,
            'greeting'=>$_POST["greeting"],
            'acknowledgement'=>$_POST["acknowledgement"],
            'active_listening_skills'=>$_POST["active_listening_skills"],
            'personalisation'=>$_POST["personalisation"],
            'displaye_empathy'=>$_POST["displaye_empathy"],
            'communication_skill'=>$_POST["communication_skill"],
            'was_polite'=>$_POST["was_polite"],
            'proper_usage'=>$_POST["proper_usage"],
            'maintained_liveliness'=>$_POST["maintained_liveliness"],
            'adhered_script'=>$_POST["adhered_script"],
            'handled_objections'=>$_POST["handled_objections"],
            'how_well_did'=>$_POST["how_well_did"],
            'website_promotion'=>$_POST["website_promotion"],
            'shared_resort'=>$_POST["shared_resort"],
            'pre_call_work_done'=>$_POST["pre_call_work_done"],
            'being_proactive'=>$_POST["being_proactive"],
            'CAPS_promotion'=>$_POST["CAPS_promotion"],
            'social_media'=>$_POST["social_media"],
            'closing_script'=>$_POST["closing_script"],
            'no_misrepresentation'=>$_POST["no_misrepresentation"],
            'complete_accurate'=>$_POST["complete_accurate"],
            'callback_follow_up'=>$_POST["callback_follow_up"],
            'non_contactable'=>$_POST["non_contactable"],
            'ownership'=>$_POST["ownership"],
            'rude_on_call'=>$_POST["rude_on_call"],
            'call_synopsis'=>$_POST["call_synopsis"],
            'strength'=>$_POST["strength"],
            'area_of_opportunity'=>$_POST["area_of_opportunity"],
            'total_score'=>$_POST["total_score"],
            'score_category'=>$_POST["score_category"],
             'created_on' => $created_on,
            'created_by' => $created_by,
            
        );
         $this->db->insert('tbl_holiday_paln_audit_sheet_category', $array2);
    }
       
    public function add_file_db($data_user) {
        $dept=$this->session->userdata('user_name');
        $data_user['created_by']=$dept;
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');
        $data_user['created_on']=$date1;
        $this->db->insert('tbl_file_upload', $data_user);
        return $this->db->insert_id();
    }
    
    
    public function save_data_form()
    {
       $data=$_POST; 
       
       $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
       $date1 = $date->format('Y-m-d');
      
       
       for ($i = 0; $i < count($data['parameter']); $i++) 
       {
            $b = array(
                'ticket' => $data['ticket'],
                'parameter' => $data['parameter'][$i],
                'expection_rate' => $data['expection_rate'][$i],
                'experience_rate' => $data['experience_rate'][$i],
                'remark' => $data["remark"][$i],
                'expection_rate_score' => $data["expection_rate_score"],
                'experience_rate_score' => $data["experience_rate_score"],
                'expection_rate_csi' => $data["expection_rate_csi"],
                'experience_rate_csi' => $data["experience_rate_csi"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
            );
            $this->db->insert('tbl_question_psf', $b);
        }
        
        $z = array(
                'ticket' => $data["ticket"],
                'quest_1' => $data["quest_1"],
                'quest_2' => $data["quest_2"],
                'quest_3' => $data["quest_3"],
                'quest_4' => $data["quest_4"],
                'email_id' => $data["email_id"],
                'call_status' => $data["call_status"],
                'if_contacted' => $data["if_contacted"]." ".$data["follow_date"],
                'not_contacted' => $data["not_contacted"],
                'csa_name' => $this->session->userdata('user_name'),
                'date_calling' => $date1,
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1
            );
//            print_r($z);
//            exit();
        
           $this->db->insert('tbl_another_question', $z);
        
            
    }

}
