<?php

class login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        
    }

    public function login() {

        $username = $this->input->post('username');
        $password = $this->input->post('pass');
        $this->db->select('*');
        $this->db->from('tbl_user');
        $this->db->where('name', $username);
        $this->db->where('password', $password);
        return $this->db->get()->row();
    }
    
    public function save_data() {

        $data = $_POST;
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');

        if (!empty($data['hadness_id'])) {
            $hardness_id = $data['hadness_id'];

            // DELETE HARDNESS ID
            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_b');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_para');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_para_b');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_observation');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_observation_b');




            // UPDATE HARDNESS MAIN
            $update_data = array(
                'c_date' => $data['c_date'],
                'part_no' => $data['part_no'],
                'approved_by' => $data['approved_by'],
                'checked_by' => $data['checked_by'],
                'status' => $data['status'],
                'remark' => $data['remark']
            );
            $this->db->where('id', $hardness_id);
            $this->db->update('tbl_hardness_main', $update_data);
            //echo $hardness_id;
            //exit();
            // END UPDATE HARDNESS MAIN



            for ($j = 0; $j < count($data['L']); $j++) {
                $l = implode(",", $data["L" . $j]);
                $c = array(
                    'hardness_chill_id' => $hardness_id,
                    'L' => $data["L"][$j],
                    'L_C' => $l,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation', $c);
            }
            for ($i = 0; $i < count($data['working_spec']); $i++) {
                $b = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point'][$i],
                    'cust_spec' => $data['cust_spec'][$i],
                    'working_spec' => $data['working_spec'][$i],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para', $b);
            }
            for ($m = 0; $m < count($data['L']); $m++) {
                $aa[] = implode(",", $data["L" . $m]);
            }
            $ab = '';
            for ($a = 0; $a < count($aa); $a++) {
                $ab .= $aa[$a] . ",";
            }
            $ab = explode(",", rtrim($ab, ","));
            for ($c = 0; $c < count($ab); $c++) {
                $cc = implode(",", $data["a" . $c]);
                $bb = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab[$c],
                    'data_value' => $cc,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth', $bb);
            }

            // -----B----


            for ($j1 = 0; $j1 < count($data['LB']); $j1++) {
                $l1 = implode(",", $data["LB" . $j1]);
                $c1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'L' => $data["LB"][$j1],
                    'L_C' => $l1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation_b', $c1);
            }
            for ($i1 = 0; $i1 < count($data['at_point_b']); $i1++) {
                $b1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point_b'][$i1],
                    'cust_spec' => $data['cust_spec_b'][$i1],
                    'working_spec' => $data['working_spec_b'][$i1],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para_b', $b1);
            }
            for ($m1 = 0; $m1 < count($data['LB']); $m1++) {
                $aa1[] = implode(",", $data["LB" . $m1]);
            }
            $ab1 = '';
            for ($a1 = 0; $a1 < count($aa1); $a1++) {
                $ab1 .= $aa1[$a1] . ",";
            }
            $ab1 = explode(",", rtrim($ab1, ","));
            for ($c1 = 0; $c1 < count($ab1); $c1++) {
                $cc1 = implode(",", $data["b" . $c1]);
                $bb1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab1[$c1],
                    'data_value' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_b', $bb1);
            }
            echo $hardness_id;
        }

        // -------------------------------------------------------------------------------  
        else {

            //echo "NO";
            // ELSE ID NOT EXIST
            $a = array(
                'item' => $data['item'],
                'c_date' => $data['c_date'],
                'heat_no' => $data['heat_no'],
                'part_no' => $data['part_no'],
                'checked_by' => $data["checked_by"],
                'approved_by' => $data["approved_by"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
                'status' => $data['status'],
                'remark' => $data['remark']
            );
            $this->db->insert('tbl_hardness_main', $a);
            $insert_id = $this->db->insert_id();

            for ($j = 0; $j < count($data['L']); $j++) {
                $l = implode(",", $data["L" . $j]);
                $c = array(
                    'hardness_chill_id' => $insert_id,
                    'L' => $data["L"][$j],
                    'L_C' => $l,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation', $c);
            }
            for ($i = 0; $i < count($data['working_spec']); $i++) {
                $b = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point'][$i],
                    'cust_spec' => $data['cust_spec'][$i],
                    'working_spec' => $data['working_spec'][$i],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para', $b);
            }
            for ($m = 0; $m < count($data['L']); $m++) {
                $aa[] = implode(",", $data["L" . $m]);
            }

            $ab = '';
            for ($a = 0; $a < count($aa); $a++) {
                $ab .= $aa[$a] . ",";
            }
            $ab = explode(",", rtrim($ab, ","));
            for ($c = 0; $c < count($ab); $c++) {
                $cc = implode(",", $data["a" . $c]);
                $bb = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab[$c],
                    'data_value' => $cc,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth', $bb);
            }

            // -----B----


            for ($j1 = 0; $j1 < count($data['LB']); $j1++) {
                $l1 = implode(",", $data["LB" . $j1]);
                $c1 = array(
                    'hardness_chill_id' => $insert_id,
                    'L' => $data["LB"][$j1],
                    'L_C' => $l1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation_b', $c1);
            }
            for ($i1 = 0; $i1 < count($data['at_point_b']); $i1++) {
                $b1 = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point_b'][$i1],
                    'cust_spec' => $data['cust_spec_b'][$i1],
                    'working_spec' => $data['working_spec_b'][$i1],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para_b', $b1);
            }
            for ($m1 = 0; $m1 < count($data['LB']); $m1++) {
                $aa1[] = implode(",", $data["LB" . $m1]);
            }

            $ab1 = '';
            for ($a1 = 0; $a1 < count($aa1); $a1++) {
                $ab1 .= $aa1[$a1] . ",";
            }
            $ab1 = explode(",", rtrim($ab1, ","));
            $cc1 = '';
            for ($c1 = 0; $c1 < count($ab1); $c1++) {
                $cc1 = implode(",", $data["b" . $c1]);

                $bb1 = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab1[$c1],
                    'data_value' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_b', $bb1);
            }
            echo $insert_id;
        }
        // END 
        // -----/B-------
        // echo "abc";
    }

    public function save_data_670() {

        $data = $_POST;
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');

        if (!empty($data['hadness_id'])) {
            $hardness_id = $data['hadness_id'];

            // DELETE HARDNESS ID
            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_b');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_para');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_chilldepth_para_b');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_observation');

            $this->db->where('hardness_chill_id', $hardness_id);
            $this->db->delete('tbl_hardness_observation_b');




            // UPDATE HARDNESS MAIN
            $update_data = array(
                'c_date' => $data['c_date'],
                'part_no' => $data['part_no'],
                'approved_by' => $data['approved_by'],
                'checked_by' => $data['checked_by'],
                'status' => $data['status'],
                'remark' => $data['remark']
            );
            $this->db->where('id', $hardness_id);
            $this->db->update('tbl_hardness_main', $update_data);
            //echo $hardness_id;
            //exit();
            // END UPDATE HARDNESS MAIN



            for ($j = 0; $j < count($data['L']); $j++) {
                $l = implode(",", $data["L" . $j]);
                $c = array(
                    'hardness_chill_id' => $hardness_id,
                    'L' => $data["L"][$j],
                    'L_C' => $l,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation', $c);
            }
            for ($i = 0; $i < count($data['working_spec']); $i++) {
                $b = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point'][$i],
                    'cust_spec' => $data['cust_spec'][$i],
                    'working_spec' => $data['working_spec'][$i],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para', $b);
            }
            for ($m = 0; $m < count($data['L']); $m++) {
                $aa[] = implode(",", $data["L" . $m]);
            }
            $ab = '';
            for ($a = 0; $a < count($aa); $a++) {
                $ab .= $aa[$a] . ",";
            }
            $ab = explode(",", rtrim($ab, ","));
            for ($c = 0; $c < count($ab); $c++) {
                $cc = implode(",", $data["a" . $c]);
                $cc1 = implode(",", $data["ab" . $c]);
                $bb = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab[$c],
                    'data_value' => $cc,
                    'data_value1' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth', $bb);
            }

            // -----B----


            for ($j1 = 0; $j1 < count($data['LB']); $j1++) {

                $c1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'L' => $data["LB"][$j1],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation_b', $c1);
            }
            for ($i1 = 0; $i1 < count($data['at_point_b']); $i1++) {
                $b1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point_b'][$i1],
                    'cust_spec' => $data['cust_spec_b'][$i1],
                    'working_spec' => $data['working_spec_b'][$i1],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para_b', $b1);
            }

            for ($c1 = 0; $c1 < count($data['LB']); $c1++) {
                $cc1 = implode(",", $data["b" . $c1]);
                $bb1 = array(
                    'hardness_chill_id' => $hardness_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    //'L_C' => $ab1[$c1],
                    'data_value' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_b', $bb1);
            }
            echo $hardness_id;
        }

        // -------------------------------------------------------------------------------  
        else {

            //echo "NO";
            // ELSE ID NOT EXIST
            $a = array(
                'item' => $data['item'],
                'c_date' => $data['c_date'],
                'heat_no' => $data['heat_no'],
                'part_no' => $data['part_no'],
                'checked_by' => $data["checked_by"],
                'approved_by' => $data["approved_by"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
                'status' => $data['status'],
                'remark' => $data['remark']
            );
            $this->db->insert('tbl_hardness_main', $a);
            $insert_id = $this->db->insert_id();

            for ($j = 0; $j < count($data['L']); $j++) {
                $l = implode(",", $data["L" . $j]);
                $c = array(
                    'hardness_chill_id' => $insert_id,
                    'L' => $data["L"][$j],
                    'L_C' => $l,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation', $c);
            }
            for ($i = 0; $i < count($data['working_spec']); $i++) {
                $b = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point'][$i],
                    'cust_spec' => $data['cust_spec'][$i],
                    'working_spec' => $data['working_spec'][$i],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para', $b);
            }
            for ($m = 0; $m < count($data['L']); $m++) {
                $aa[] = implode(",", $data["L" . $m]);
            }

            $ab = '';
            for ($a = 0; $a < count($aa); $a++) {
                $ab .= $aa[$a] . ",";
            }
            $ab = explode(",", rtrim($ab, ","));
            for ($c = 0; $c < count($ab); $c++) {
                $cc = implode(",", $data["a" . $c]);
                $cc1 = implode(",", $data["ab" . $c]);
                $bb = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'L_C' => $ab[$c],
                    'data_value' => $cc,
                    'data_value1' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth', $bb);
            }

            // -----B----


            for ($j1 = 0; $j1 < count($data['LB']); $j1++) {
                //$l1 = implode(",", $data["LB" . $j1]);
                $c1 = array(
                    'hardness_chill_id' => $insert_id,
                    'L' => $data["LB"][$j1],
                    //'L_C' => $l1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_observation_b', $c1);
            }
            for ($i1 = 0; $i1 < count($data['at_point_b']); $i1++) {
                $b1 = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    'at_point' => $data['at_point_b'][$i1],
                    'cust_spec' => $data['cust_spec_b'][$i1],
                    'working_spec' => $data['working_spec_b'][$i1],
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_para_b', $b1);
            }

            $cc1 = '';
            for ($c1 = 0; $c1 < count($data['LB']); $c1++) {
                $cc1 = implode(",", $data["b" . $c1]);

                $bb1 = array(
                    'hardness_chill_id' => $insert_id,
                    'item' => $data['item'],
                    'c_date' => $data['c_date'],
                    'heat_no' => $data['heat_no'],
                    //'L_C' => $ab1[$c1],
                    'data_value' => $cc1,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_hardness_chilldepth_b', $bb1);
            }
            echo $insert_id;
        }
        // END 
        // -----/B-------
    }
    
     public function get_all_heat()
    {
        $query="select heat_no from tbl_hardness_main group by heat_no";
        $result=$this->db->query($query)->result();
        if($result)
        {
          return $result;  
        }
    }

    public function overall_dashboard()
    {
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1=$date->format('Y-m-d');
         $query = " select m.m1,p.p1,f.f1,pa.pa1
    from			
(select SUM(total_mould) as m1 from  tbl_camshaft_scrap Where created_on ='$date1') as m,
(select SUM(total_pouring) as p1 from  tbl_camshaft_scrap Where created_on ='$date1') as p,
(select SUM(total_fettling) as f1 from  tbl_camshaft_scrap Where created_on ='$date1') as f,
(select SUM(total_pattern) as pa1 from  tbl_camshaft_scrap Where created_on ='$date1') as pa";

         return $this->db->query($query)->row();
    }
    
    // CAMSHAFT SCRAP
    public function save_camshaft_scrap() {

        $data = $_POST;
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');

        if (!empty($data['cam_id'])) {
            $cam_id = $data['cam_id'];

            // DELETE cam_id ID
            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_fettling_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_melting_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_moulding_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_pattern_deffects');
            // UPDATE HARDNESS MAIN
            $update_data = array(
                'inspector' => $data['inspector'],
                'part_no' => $data['part_no'],
                'cs' => $data['cs'],
                'item' => $data['item'],
                'shift' => $data['shift'],
                'c_date' => $data['c_date'],
                'total_checked' => $data['total_checked'],
                'total_good' => $data["total_good"],
                'total_rej' => $data["total_rej"],
                'fr_cut' => $data["fr_cut"],
                'met_rej' => $data["met_rej"],
                'total_mould' => $data["total_mould"],
                'total_pouring' => $data["total_pouring"],
                'total_fettling' => $data["total_fettling"],
                'total_pattern' => $data["total_pattern"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
                'status' => $data['status']
            );
            $this->db->where('id', $cam_id);
            $this->db->update('tbl_camshaft_scrap', $update_data);


            // MOULD
            $mould_name= array(
            'LOOSE SAND','PIN HOLE','GAS HOLE','GLUE','MOULD CRACK','CORE BROKEN','TIR REJECT',
            'CHILL DEFECT','THICK FLASH','MOULD LEAK','EXTRA METAL','OTHER');  
        
            $total_defects_by_mould = implode(",", $data["total_defects_by_mould"]);
            $imp_no= implode(",", $data['imp_no_mould']);
            for ($i = 0; $i < count($mould_name); $i++) {
                $defects = implode(",", $data["defects_mould_a" . $i]);
                $defects_sum= array_sum($data["defects_mould_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $mould_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_mould,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_moulding_deffects', $a1);
            }

            // END MOULD
            // MELT
            $melt_name=array(
            'HOT TEAR','COLD METAL','LC','SLAG','RIPPLE','SHORT POURED','PELTCH','OTHER'
        );
        
        
            $total_defects_by_melt = implode(",", $data["total_defects_by_melt"]);
            for ($i = 0; $i < count($melt_name); $i++) {
                $defects = implode(",", $data["defects_melt_a" . $i]);
                $defects_sum= array_sum($data["defects_melt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' =>$imp_no,
                    'defect_name' => $melt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_melt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_melting_deffects', $a1);
            }

            // END MELT
            // FELT
            $felt_name=array('CHIP COLD','CHIPHOT','EXCESS FET.','OTHER');
        
            $total_defects_by_felt = implode(",", $data["total_defects_by_felt"]);
            for ($i = 0; $i < count($felt_name); $i++) {
                $defects = implode(",", $data["defects_felt_a" . $i]);
                $defects_sum= array_sum($data["defects_felt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $felt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_felt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_fettling_deffects', $a1);
            }

            // END FELT
            // PATTERN
        $pattern_name=array('X - JOINT','DIGGING IN','LINEAR GAUGE REJ.','OTHER ( SIM GAP, P/M ETC.)','PPAP');
            $total_defects_by_pattern = implode(",", $data["total_defects_by_pattern"]);
            for ($i = 0; $i < count($pattern_name); $i++) {
                $defects = implode(",", $data["defects_pattern_a" . $i]);
                $defects_sum= array_sum($data["defects_pattern_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $pattern_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_pattern,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_pattern_deffects', $a1);
            }
            echo $cam_id;
        } else {



            $b = array(
                'inspector' => $data['inspector'],
                'part_no' => $data['part_no'],
                'cs' => $data['cs'],
                'item' => $data['item'],
                'shift' => $data['shift'],
                'c_date' => $data['c_date'],
                'total_checked' => $data['total_checked'],
                'total_good' => $data["total_good"],
                'total_rej' => $data["total_rej"],
                'fr_cut' => $data["fr_cut"],
                'met_rej' => $data["met_rej"],
                'total_mould' => $data["total_mould"],
                'total_pouring' => $data["total_pouring"],
                'total_fettling' => $data["total_fettling"],
                'total_pattern' => $data["total_pattern"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
                'status' => $data['status']
            );
            $this->db->insert('tbl_camshaft_scrap', $b);
            $insert_id = $this->db->insert_id();

            // MOULD
            $mould_name= array(
            'LOOSE SAND','PIN HOLE','GAS HOLE','GLUE','MOULD CRACK','CORE BROKEN','TIR REJECT',
            'CHILL DEFECT','THICK FLASH','MOULD LEAK','EXTRA METAL','OTHER');  
        
            $total_defects_by_mould = implode(",", $data["total_defects_by_mould"]);
            $imp_no= implode(",", $data['imp_no_mould']);
            for ($i = 0; $i < count($mould_name); $i++) {
                $defects = implode(",", $data["defects_mould_a" . $i]);
                $defects_sum= array_sum($data["defects_mould_a" . $i]);


                $a1 = array(
                    'camshaft_scrap_id' => $insert_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $mould_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_mould,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_moulding_deffects', $a1);
            }

            // END MOULD
            // MELT
            $melt_name=array(
            'HOT TEAR','COLD METAL','LC','SLAG','RIPPLE','SHORT POURED','PELTCH','OTHER'
        );
        
        
            $total_defects_by_melt = implode(",", $data["total_defects_by_melt"]);
            for ($i = 0; $i < count($melt_name); $i++) {
                $defects = implode(",", $data["defects_melt_a" . $i]);
                $defects_sum= array_sum($data["defects_melt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $insert_id,
                    'imp_no' =>$imp_no,
                    'defect_name' => $melt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_melt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_melting_deffects', $a1);
            }

            // END MELT
            // FELT
            $felt_name=array('CHIP COLD','CHIPHOT','EXCESS FET.','OTHER');
        
            $total_defects_by_felt = implode(",", $data["total_defects_by_felt"]);
            for ($i = 0; $i < count($felt_name); $i++) {
                $defects = implode(",", $data["defects_felt_a" . $i]);
                $defects_sum= array_sum($data["defects_felt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $insert_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $felt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_felt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_fettling_deffects', $a1);
            }

            // END FELT
            // PATTERN
        $pattern_name=array('X - JOINT','DIGGING IN','LINEAR GAUGE REJ.','OTHER ( SIM GAP, P/M ETC.)','PPAP');
            $total_defects_by_pattern = implode(",", $data["total_defects_by_pattern"]);
            for ($i = 0; $i < count($pattern_name); $i++) {
                $defects = implode(",", $data["defects_pattern_a" . $i]);
                $defects_sum= array_sum($data["defects_pattern_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $insert_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $pattern_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_pattern,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_pattern_deffects', $a1);
            }

            echo $insert_id;
        }
        // END PATTERN
    }

    public function edit_camshaft_scrap_data() {

        $data = $_POST;
        
        $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
        $date1 = $date->format('Y-m-d');

        if (!empty($data['cam_id'])) {
            $cam_id = $data['cam_id'];

            // DELETE HARDNESS ID
            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_fettling_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_melting_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_moulding_deffects');

            $this->db->where('camshaft_scrap_id', $cam_id);
            $this->db->delete('tbl_cam_pattern_deffects');
            // UPDATE HARDNESS MAIN
            $update_data = array(
                'inspector' => $data['inspector'],
                'part_no' => $data['part_no'],
                'cs' => $data['cs'],
                'item' => $data['item'],
                'shift' => $data['shift'],
                'c_date' => $data['c_date'],
                'total_checked' => $data['total_checked'],
                'total_good' => $data["total_good"],
                'total_rej' => $data["total_rej"],
                'fr_cut' => $data["fr_cut"],
                'met_rej' => $data["met_rej"],
                'total_mould' => $data["total_mould"],
                'total_pouring' => $data["total_pouring"],
                'total_fettling' => $data["total_fettling"],
                'total_pattern' => $data["total_pattern"],
                'created_by' => $this->session->userdata('user_name'),
                'created_on' => $date1,
                'status' => $data['status']
            );
            $this->db->where('id', $cam_id);
            $this->db->update('tbl_camshaft_scrap', $update_data);




            // MOULD
            $mould_name= array(
            'LOOSE SAND','PIN HOLE','GAS HOLE','GLUE','MOULD CRACK','CORE BROKEN','TIR REJECT',
            'CHILL DEFECT','THICK FLASH','MOULD LEAK','EXTRA METAL','OTHER');  
        
            $total_defects_by_mould = implode(",", $data["total_defects_by_mould"]);
            $imp_no= implode(",", $data['imp_no_mould']);
            for ($i = 0; $i < count($mould_name); $i++) {
                $defects = implode(",", $data["defects_mould_a" . $i]);
                $defects_sum= array_sum($data["defects_mould_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $mould_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_mould,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_moulding_deffects', $a1);
            }

            // END MOULD
            // MELT
            $melt_name=array(
            'HOT TEAR','COLD METAL','LC','SLAG','RIPPLE','SHORT POURED','PELTCH','OTHER'
        );
        
        
            $total_defects_by_melt = implode(",", $data["total_defects_by_melt"]);
            for ($i = 0; $i < count($melt_name); $i++) {
                $defects = implode(",", $data["defects_melt_a" . $i]);
                $defects_sum= array_sum($data["defects_melt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' =>$imp_no,
                    'defect_name' => $melt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_melt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_melting_deffects', $a1);
            }

            // END MELT
            // FELT
            $felt_name=array('CHIP COLD','CHIPHOT','EXCESS FET.','OTHER');
        
            $total_defects_by_felt = implode(",", $data["total_defects_by_felt"]);
            for ($i = 0; $i < count($felt_name); $i++) {
                $defects = implode(",", $data["defects_felt_a" . $i]);
                $defects_sum= array_sum($data["defects_felt_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $felt_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_felt,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_fettling_deffects', $a1);
            }

            // END FELT
            // PATTERN
        $pattern_name=array('X - JOINT','DIGGING IN','LINEAR GAUGE REJ.','OTHER ( SIM GAP, P/M ETC.)','PPAP');
            $total_defects_by_pattern = implode(",", $data["total_defects_by_pattern"]);
            for ($i = 0; $i < count($pattern_name); $i++) {
                $defects = implode(",", $data["defects_pattern_a" . $i]);
                $defects_sum= array_sum($data["defects_pattern_a" . $i]);

                $a1 = array(
                    'camshaft_scrap_id' => $cam_id,
                    'imp_no' => $imp_no,
                    'defect_name' => $pattern_name[$i],
                    'defects' => $defects,
                    'defects_sum' => $defects_sum,
                    'total' => $total_defects_by_pattern,
                    'created_by' => $this->session->userdata('user_name'),
                    'created_on' => $date1,
                );
                $this->db->insert('tbl_cam_pattern_deffects', $a1);
            }

            // END PATTERN
        }
    }
    
    
    public function item_db_data()
    {
        $this->DB2 = $this->load->database('melt_db', TRUE);
        $query="select ipcs_item from tbl_ipcs_sheet group by ipcs_item order by id DESC";
        return $this->DB2->query($query)->result();
        $this->DB2->close();
    }

    // END CAMSHAFT SCRAP

}
